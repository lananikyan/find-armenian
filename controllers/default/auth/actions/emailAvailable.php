<?php
if(!empty($_POST["r"]["email"])){
   try{
    $filter = new UsersFilter();
    $filter->setEmail($_POST["r"]["email"]);
    Reg::get('userMgr')->getUser($filter);
    $emailIsAvailable = false;
  }
   catch(UserNotFoundException $e){
     $emailIsAvailable = true;
   }
    Reg::get('uo')->set('isAvailable', $emailIsAvailable);
}
else{
    Reg::get('error')->add("Email is not set!");
}

