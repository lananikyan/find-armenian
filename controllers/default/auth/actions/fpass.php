<?php
Reg::get('formKey')->validate((isset($_POST['key']) ? $_POST['key'] : null));
if(empty($_POST['passEmail'])){
    Reg::get('error')->add(ENTER_EMAIL);
}
else{
	try{
		$filter = new UsersFilter();
		$filter->setEmail($_POST["passEmail"]);
		$user =  Reg::get('userMgr')->getUser($filter);
		if($user->props->hostId != Reg::get('host')->id){
			$user->props->hostId = Reg::get('host')->id;
			$user->props->langId = Reg::get('language')->id;
			Reg::get('userMgr')->updateUser($user);
		}
		// Send welcome e-mail
		$reset_form_url = "http://" . Reg::get('host')->host . SITE_PATH . "auth/reset_password_form/r_id:".generateForgetPassCode($user);
		$content = Reg::get('smarty')->getChunk("mails/password_recover.tpl", array('user'=>$user, 'reset_form_url'=>$reset_form_url));
		$email = Reg::get('smarty')->getChunk("mails/template.tpl", array('mailTitle'=>"Set your new password", 'mailContent'=>$content));

		if(mail($user->email, $user->login . ", set new FindArmenian.com password", $email, mailHeaders())){
			Reg::get('info')->add(MAIL_FORG_PASS_SUCCESS_SENT);
		}
	}
	catch(UserNotFoundException $e){
		Reg::get('error')->add(ERR_USER_DOES_NOT_EXIST);
	}
}
redirect(Reg::get('rewriteURL')->glink('auth/fpass'));