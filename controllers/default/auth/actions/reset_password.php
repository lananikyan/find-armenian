<?php
Reg::get('formKey')->validate((isset($_POST['key']) ? $_POST['key'] : null));


if($user = validateForgotPassCode($_POST['c_i'])) {

	if($_POST['password'] != $_POST['password2']){
		Reg::get('error')->add(ERR_PASS_NOT_PASS2);
	}
	if(strlen($_POST['password']) < 4 or strlen($_POST['password']) > 32){
		Reg::get('error')->add(ERR_INC_PASS_LEN);
	}
	if(Reg::get('error')->isEmptyQueue()){
		if(!Reg::get('userMgr')->setUserPassword($user, $_POST['password'])){
			Reg::get('error')->add(UNEXPECTED_ERROR);
			redirect(SITE_PATH."password_recovery/recovery_pass_step1/r_id:".$_POST['c_i']);
		}
		else{
			if(removeForgetPassCode($_POST['c_i'])){
				Reg::get('info')->add(NEW_PASS_IS_SET);
				redirect(SITE_PATH.'auth');
			}
		}
	}
	else{
		redirect(SITE_PATH."password_recovery/recovery_pass_step1/r_id:".$_POST['c_i']);
	}
}
Reg::get('error')->add(UNEXPECTED_ERROR);
redirect(SITE_PATH);
