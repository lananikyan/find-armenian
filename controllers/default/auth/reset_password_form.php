<?php
if(empty($_GET['r_id'])){
	Reg::get('error')->add(FORG_PASS_ERR_CODE);
	redirect(SITE_PATH);
}
if($user = validateForgotPassCode($_GET['r_id'])){
	Reg::get('smarty')->addJs('jqueryPlugins/jquery.validate.min.js');
	/*if(Reg::get('language')->shortName != "en"){
		Reg::get('smarty')->addJs("libs/jQuery/plugins/jquery-validate/localization/messages_".Reg::get('language')->shortName . ".js");
	}*/
}
else{
	Reg::get('error')->add(FORG_PASS_ERR_CODE);
	redirect(SITE_PATH);
}