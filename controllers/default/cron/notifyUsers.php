<?php
$log = "";
// Get locations added in last hour
$newLocationDate = date(DEFAULT_DATE_FORMAT, time() - 60*60);
$newLocationTime = date(DEFAULT_TIME_FORMAT, time() - 60*60);

$newLocationsFilter = new UserLocationFilter();
$newLocationsFilter->setDateGreaterEqual($newLocationDate);
$newLocationsFilter->setTimeGreaterEqual($newLocationTime);
$newLocations = Reg::get('usrLocationMgr')->getLocations($newLocationsFilter);

// Cities where new locations are created
$cities = array();
// Countries where new locations are created
$countries = array();

foreach ($newLocations as $newLocation){
	$user = Reg::get('userMgr')->getUserById($newLocation->userId, UserManager::INIT_PROPERTIES);
	$cities[$newLocation->city][$user->props->sex][] = array('location'=>$newLocation, 'user'=>$user);
	//$countries[$newLocation->countryISO][] = array('location'=>$newLocation, 'user'=>$user);

}

//print_r($cities);
//print_r($countries);
$failedMailsCount = 0;
$failedRecipients = array();
// Send e-mail to users who registered before and asked to be notify about new users
// 1. Get users
// 2. Send email
foreach ($cities as $city=>$cityData){
	// Get user from this city, who registered before and asked to be
  // notify for new boys $usersInterestedInBoys or girls $usersInterestedInGirls
	$cityFilter = new UserLocationFilter();
	$cityFilter->setDefault();
	$cityFilter->setDateLess($newLocationDate);
	$cityFilter->setTimeLess($newLocationTime);
	$cityFilter->setCity($city);

	$interestedInBoysFilter = new SiteUserFilter();
	$interestedInBoysFilter->setAlertBoy();
	$fm = new FilterMerger($interestedInBoysFilter);
	$fm->mergeRight($cityFilter);
	$usersInterestedInBoys = Reg::get('userMgr')->getUsersList($interestedInBoysFilter);

	$interestedInGirlsFilter = new SiteUserFilter();
	$interestedInGirlsFilter->setAlertGirl();
  $fm = new FilterMerger($interestedInGirlsFilter);
  $fm->mergeRight($cityFilter);
	$usersInterestedInGirls = Reg::get('userMgr')->getUsersList($interestedInGirlsFilter);

  // Send e-mails to users interested in boys
  $log .= "\n\n ========= New location added at $city ==========\n";
  if (isset($cityData['m']) and !empty($usersInterestedInBoys)){
    $log .= "\n ======= Email to users interested in boys ====\n";
    $log .= "New male user(s) is(are) registered in " . $cityData['m'][0]['location']->city . "\n";
    foreach ($cityData['m'] as $newUserLocation){
      $log .= $newUserLocation['user']->login . " at " .$newUserLocation['location']->name . "\n";
    }
    $notificationEmail = create_notification_email($cityData['m']);
    $successMailsCount = 0;
    $successRecipients= array();
    foreach($usersInterestedInBoys as $userInterestedInBoys) {
      // Send notification e-mail
      $email = Reg::get('smarty')->getChunk("mails/template.tpl", array('mailTitle'=>$userInterestedInBoys->login . ",  new registrations in " . $city, 'mailContent'=>$notificationEmail));
      if (mail($userInterestedInBoys->email, "New user(s) are registered in the " . $city, $email, mailHeaders())){
        $successMailsCount++;
        $successRecipients[] = $userInterestedInBoys->id.":".$userInterestedInBoys->login . "(".$userInterestedInBoys->email.")";
      }
      else{
        $failedMailsCount++;
        $failedRecipients[] = $userInterestedInBoys->id.":".$userInterestedInBoys->login . "(".$userInterestedInBoys->email.")";
      }
    }
    $log .= $successMailsCount . " email(s) sent to: " . implode(", ", $successRecipients);
  }

  // Send e-mails to users interested in girls
  if (isset($cityData['f']) and !empty($usersInterestedInGirls)) {
    $log .= "\n ======= Email to users interested in girls ====\n";
    $log .= "New male user(s) is(are) registered in " . $cityData['f'][0]['location']->city . "\n";
    foreach ($cityData['f'] as $newUserLocation){
      $log .= $newUserLocation['user']->login . " at " .$newUserLocation['location']->name . "\n";
    }
    $notificationEmail = create_notification_email($cityData['f']);
    $successMailsCount = 0;
    $successRecipients= array();
    foreach ($usersInterestedInGirls as $userInterestedInGirls) {
      // Send notification e-mail
      $email = Reg::get('smarty')->getChunk("mails/template.tpl", array('mailTitle'=>$userInterestedInGirls->login . ",  new registrations in " . $city, 'mailContent'=>$notificationEmail));
      if (mail($userInterestedInGirls->email, "New user(s) are registered in the " . $city, $email, mailHeaders())){
        $successMailsCount++;
        $successRecipients[] = $userInterestedInGirls->id.":".$userInterestedInGirls->login . "(".$userInterestedInGirls->email.")";
      }
      else{
        $failedMailsCount++;
        $failedRecipients[] = $userInterestedInBoys->id.":".$userInterestedInBoys->login . "(".$userInterestedInBoys->email.")";
      }
    }
    $log .= $successMailsCount . " email(s) sent to: " . implode(", ", $successRecipients);

  }
  $log .= "\n ==================================\n";

}

if ($failedMailsCount > 0){
  $log .= "\nTotal failed mails: " . $failedMailsCount;
  $log .= "\nFailed recipients: " . implode(", ", $failedRecipients);
  $log .= "\n";
}

if (!empty($log)) {
  mail("lev.ananikyan@gmail.com", "New users cron", $log);
}

