<?php
// Get ALL locations to show on map
$locations = Reg::get("usrLocationMgr")->getLocations();
$pins = array();
foreach($locations as $location){
	$pin = array();
	$pin['id'] = $location->id;
	$pin['lat'] = $location->lat;
	$pin['lng'] = $location->lng;
	$pin['type'] = $location->type;
	$pin['isBusiness'] = $location->isBusiness;
  if (isAuthorized()) {
    if ($location->userId == Reg::get('usr')->id ){
      $pin['my'] = true;
    }
  }
	$pins[] = $pin;
}
Reg::get('smarty')->addCustomHeadTag('<script type="text/javascript">var pins = '.json_encode($pins).'</script>');

// Get last registered users list with default location
$countryISO = empty($_GET['country']['iso2']) ? null : $_GET['country']['iso2'];
$thisCountryLocations = Reg::get("usrLocationMgr")->getUsersInCountry(20, $countryISO);
Reg::get("smarty")->assign("locations", array_reverse($thisCountryLocations));

if (!empty($_GET['country'])) {
  Reg::get("smarty")->setPageTitle("Armenians of " . $_GET['country']['name_en'] . " :: " . Reg::get("smarty")->getPageTitle());
  Reg::get("smarty")->setPageKeywords("Armenians of " . $_GET['country']['name_en'] . ", " . Reg::get("smarty")->getKeywords());
  Reg::get("smarty")->setPageDescription("Armenians of " . $_GET['country']['name_en'] . ". " . Reg::get("smarty")->getDescription());
}

Reg::get("smarty")->assign("countries", Reg::get('usrLocationMgr')->countries());
Reg::get('smarty')->setLayout('home', true);
Reg::get('smarty')->addPrimaryJs("http://j.maxmind.com/js/apis/geoip2/v2.0/geoip2.js");
Reg::get('smarty')->addJs('jqueryPlugins/jquery.validate.min.js');
Reg::get('smarty')->addJs('jqueryPlugins/jquery.form.js');
Reg::get('smarty')->addJs('markerclusterer/markerclusterer.js');
Reg::get('smarty')->addJs('infobox_packed.js');
Reg::get('smarty')->addJs("address_autocomplete.js");
Reg::get('smarty')->addJs("map.js");
Reg::get('smarty')->addJs("message.js");
