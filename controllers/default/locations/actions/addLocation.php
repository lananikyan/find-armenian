<?php
if (isset($_POST['latlng'])) {
	list($lat, $lng) = explode(",", $_POST['latlng']);
	if (isset($_POST['type'])) {
		try {
			$location = new UserLocation();
			/*  Set business or user location.
			   *  Set location type primary or temporary.
			  */

			if ($_POST['type'] == 'b') {
				$location->isBusiness = true;
				$location->type = 'p'; // Business location is always primary
				$location->business = new BusinessLocation();
				$location->business->phone = $_POST["phone"];
				$location->business->email = $_POST["email"];
				$location->business->url = $_POST["url"];
				$location->business->categoryId = 0; // TODO: Implement business categories
			} else {
				$location->isBusiness = false;
				$location->type = $_POST['type'];
			}

			// Set location coordinates
			$location->lat = $lat;
			$location->lng = $lng;

			// Set location name, country, address
			$location->name = $_POST['name'];
			$location->countryISO = $_POST['countryAlpha2'];
			$location->city = $_POST['city'];
			$location->googleAddress = $_POST['googleAddress'];
			$location->userAddress = $_POST['address'];

			//Set expiration date
			if (isset($_POST['expirationDate']) and !empty($_POST['expirationDate'])) {
				$location->expire = date(DEFAULT_DATE_FORMAT, strtotime($_POST['expirationDate']));
			}

			/**
			 * If user is authorized, add location
			 * otherwise save location object to session
			 * and redirect to registration
			 */
			if (isAuthorized()) {
				$location->userId = Reg::get('usr')->id;
        // TODO: "my" is not property of location object. In general AJAX response should be something different.
        // TODO: See home.php
        $location->my = true;
				//If user has no default location set this one as default
				$defaultLocation = Reg::get('usrLocationMgr')->getUserDefaultLocation(Reg::get('usr'));
				if (empty($defaultLocation)) {
					$location->isDefault = true;
				}
				Reg::get('usrLocationMgr')->addUserLocation($location);

				// Add newly created location to response
				Reg::get('uo')->set('location', $location);
				Reg::get('info')->add('Your location is successfully added');
			} else {
				$_SESSION["guestLocation"] = serialize($location);
				Reg::get('uo')->setStatusNotOk();
				Reg::get("uo")->redirect(Reg::get('rewriteURL')->glink('auth/registration'));
			}

		} catch (Exception $e) {
			Reg::get('error')->add($e->getMessage());
			Reg::get('uo')->setStatusNotOk();
		}
	} else {
		Reg::get('error')->add("Location type is non set!");
		Reg::get('uo')->setStatusNotOk();
	}
} else {
	Reg::get('error')->add("Coordinates are not set!");
	Reg::get('uo')->setStatusNotOk();
}