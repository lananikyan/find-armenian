<?php
if(isset($_POST['locationId'])){
	try{
		$location = Reg::get('usrLocationMgr')->getLocationById($_POST['locationId'], UserLocationManager::INIT_OBJECTS);
		$user = Reg::get('userMgr')->getUserById($location->userId);
		$grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( $user->email ) ) ) . "?d=identicon&s=50";
		Reg::get('uo')->assign('location', $location);
		Reg::get("uo")->assign("avatar_url", $grav_url);
		Reg::get('uo')->assign('user', $user);
	}
	catch(Exception $e){
		Reg::get('error')->add($e->getMessage());
		Reg::get('uo')->setStatusNotOk();
	}
}
else{
	Reg::get('error')->add("Location id is not set!");
	Reg::get('uo')->setStatusNotOk();
}