<?php
if (isAuthorized()) {
	try{
		$recipient = Reg::get('userMgr')->getUserById($_POST['messageToUserId'], UserManager::INIT_NONE, 10);
		$subject = "New message from " . Reg::get('usr')->login;
		$content = Reg::get('smarty')->getChunk("mails/message.tpl", array('recipient'=>$recipient, 'sender'=>Reg::get('usr'), 'message'=>$_POST['message']));
		$email = Reg::get('smarty')->getChunk("mails/template.tpl", array('mailTitle'=>"New message from " .Reg::get('usr')->login, 'mailContent'=>$content));

		if(mail($recipient->email, $subject, $email, mailHeaders())){
			Reg::get('info')->add('Your message is successfully sent');
		}
	}
	catch (Exception $e){
		Reg::get('uo')->setStatusNotOk();
		Reg::get('error')->add('Your message is not sent');
	}
} else {
	Reg::get('uo')->setStatusNotOk();
	Reg::get("uo")->redirect(Reg::get('rewriteURL')->glink('auth/registration'));
}