<?php
$infoLang 	= isset($_GET["langId"]) ? new Language($_GET["langId"]) : null;
$infoHost 	= isset($_GET["hostId"]) ? new Host($_GET["hostId"]) : null;
$infoModule = isset($_GET["moduleName"]) ? $_GET["moduleName"] : null;
$infoPage 	= isset($_GET["pageName"]) ? $_GET["pageName"] : null;

$pageInfo = PageInfoManager::getRecord($infoLang, $infoHost, $infoModule, $infoPage);
Reg::get('uo')->set('pageInfo', $pageInfo);
