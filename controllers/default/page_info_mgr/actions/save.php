<?php
$pageInfo = array("title" => $_POST["infoTitle"], "keywords" => $_POST["infoKeywords"], "description" => $_POST["infoDescription"]);
try{
	if($_POST["langId"] == "all"){
		PageInfoManager::save($pageInfo);
	}
	elseif($_POST["hostId"] == "all"){
		PageInfoManager::save($pageInfo, new Language($_POST["langId"]));
	}
	elseif($_POST["module"] == "all"){
		PageInfoManager::save($pageInfo, new Language($_POST["langId"]), new Host($_POST["hostId"]));
	}
	elseif($_POST["page"] == "all"){
		PageInfoManager::save($pageInfo, new Language($_POST["langId"]), new Host($_POST["hostId"]), $_POST["module"] );
	}
	else{
		PageInfoManager::save($pageInfo, new Language($_POST["langId"]), new Host($_POST["hostId"]),$_POST["module"], $_POST["page"] );
	}
  Reg::get('info')->add('Page information successfully saved');
}
catch (Exception $e){
  Reg::get('error')->add($e->getMessage());
  Reg::get('uo')->setStatusNotOk();
}
