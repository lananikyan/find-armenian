<?php
if(isset($_GET['locationId'])){
	try{
		$location = Reg::get('usrLocationMgr')->getLocationById($_GET['locationId']);
		if($location->userId == Reg::get('usr')->id){
			Reg::get('usrLocationMgr')->deleteUserLocation($location->id);
			Reg::get('info')->add("Location deleted");
		}
		else{
			Reg::get('error')->add("Trying to delete wrong location!");
			Reg::get('uo')->setStatusNotOk();
		}
		$user = Reg::get('userMgr')->getUserById($location->userId);

	}
	catch(Exception $e){
		Reg::get('error')->add($e->getMessage());
		Reg::get('uo')->setStatusNotOk();
	}
}
else{
	Reg::get('error')->add("Address location id is not set!");
	Reg::get('uo')->setStatusNotOk();
}
redirect(Reg::get('rewriteURL')->glink('profile'));