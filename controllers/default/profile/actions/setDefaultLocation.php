<?php
if(isset($_GET['locationId'])){
	try{
		$location = Reg::get('usrLocationMgr')->getLocationById($_GET['locationId']);
		if($location->userId == Reg::get('usr')->id){
			$oldDefaultLocation = Reg::get('usrLocationMgr')->getUserDefaultLocation(Reg::get('usr'));
			if(!empty($oldDefaultLocation)){
				$oldDefaultLocation->isDefault = false;
				$update = array();
				$addressLocationHTML = Reg::get('smarty')->getChunk('addressLocation.tpl', array("location"=>$oldDefaultLocation));
				$update['elementId'] = "location_" . $oldDefaultLocation->id;
				$update['replaceWith'] = $addressLocationHTML;
				Reg::get('uo')->set('update', $update);
				Reg::get('usrLocationMgr')->updateUserLocation($oldDefaultLocation);
			}
			$location->isDefault = true;
			Reg::get('usrLocationMgr')->updateUserLocation($location);
			Reg::get('info')->add("Default location is set");
		}
		else{
			Reg::get('error')->add("Trying to set wrong location!");
			Reg::get('uo')->setStatusNotOk();
		}

	}
	catch(Exception $e){
		Reg::get('error')->add($e->getMessage());
		Reg::get('uo')->setStatusNotOk();
	}
}
else{
	Reg::get('error')->add("Address location id is not set!");
	Reg::get('uo')->setStatusNotOk();
}