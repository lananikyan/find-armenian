<?php
$filter = new UserLocationFilter();
$filter->setUserId(Reg::get('usr')->id);
$locations = Reg::get('usrLocationMgr')->getLocations($filter);

$grav_url = "http://www.gravatar.com/avatar/" . md5( strtolower( trim( Reg::get('usr')->email ) ) ) . "?d=identicon&s=100";
$avatar_url = $grav_url;
Reg::get("smarty")->assign("avatar_url", $avatar_url);
Reg::get("smarty")->assign("locations", $locations);
Reg::get('smarty')->addJs("profile.js");
Reg::get('smarty')->addJs("address_autocomplete.js");


