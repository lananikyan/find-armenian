<?php
if(!defined('IS_CGI') and isInProductionMode() and !Reg::get('userPermsMgr')->hasPermission(Reg::get('usr'), 'tools_manager')){
	exit();
}
Reg::get('smarty')->disableOutput();
