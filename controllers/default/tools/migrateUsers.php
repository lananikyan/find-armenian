<?php
$startedAtTime = microtime(true);
ob_end_flush();
if(empty($_GET['dbname'])) die("Provide DB name to migrate from. Example:\n dbname=remote_db dbuser=remotedbuser dbpassword=remotedbpass\n");
$dbname = $_GET['dbname'];

$dbConfig = ConfigManager::getConfig("Db")->AuxConfig;

$dbUser = empty($_GET['dbuser']) ? $dbConfig->user : $_GET['dbuser'];
$dbPassword = empty($_GET['dbpassword']) ? $dbConfig->password : $_GET['dbpassword'];


$key3 = MySqlDbManager::createInstance($dbConfig->host, $dbUser, $dbPassword, $dbname, false);
$db3 = MySqlDbManager::getDbObject($key3);
$sql3 = new MySqlQuery($db3); // External DB sql object

/////////////////////////////////////////////////

$createdUsersCount = 0;
$locationsAdded = 0;
$qbSelect = new QueryBuilder();
$qbSelect->select(array(
  new Field('*', 'user'),
  new Field('name', 'pins'),
  new Field('lat', 'pins'),
  new Field('lng', 'pins'),
  new Field('city', 'pins'),
  new Field('address', 'pins'),
  new Field('is_default', 'pins'),
  new Field('expire', 'pins'),
  new Field('is_business', 'pins'),
  new Field('abr', 'c'),
  )
)->from('user')
  ->leftJoin('pins', 'pins', $qbSelect->expr()->equal(new Field('user_id', 'pins'), new Field('id', 'user')))
  ->leftJoin('country', 'c', $qbSelect->expr()->equal(new Field('id', 'c'), new Field('country_id', 'pins')));

echo $qbSelect->getSQL() ."\n";

$sql3->exec($qbSelect->getSQL());
while(($user = $sql3->fetchRecord()) != false){
  try{
    $filter = new UsersFilter();
    $filter->setEmail($user['email']);
    $usr = Reg::get('userMgr')->getUser($filter);
    $user_exists = true;
  }
  catch(UserNotFoundException $e){
    $user_exists = false;
  }

  // Create user
  if (!$user_exists) {
    $usr = new User();
    $props = new UserProperties();
    $usr->props = $props;

    $parts = explode("@", $user['email']);
    $mailUsername = $mailUsernameOriginal = $parts[0];
    $i = 0;
    while (Reg::get('userMgr')->isLoginExists($mailUsername)) {
      $mailUsername = $mailUsernameOriginal . "_" . $i++;
    }

    $usr->login = $mailUsername;
    $usr->password = $user['password'];
    $usr->email = $user['email'];
    $props->sex = $user['gender'] == 1 ? 'm' : 'f';
    $user['gender'] == 1 ? $props->emailAlertGirl = "1" : $props->emailAlertBoy = "1";

    $props->langId = Reg::get('language')->id;
    $props->hostId = Reg::get('host')->id;

    $userId = Reg::get('userMgr')->createUser($usr);
    $createdUsersCount++;
    try {
      $usr = Reg::get('userMgr')->getUserById($userId);
      Reg::get('userGroupsMgr')->addUserToGroup($usr, Reg::get('userGroupsMgr')->getGroupByName(UserGroupsManager::GROUP_USERS));
    } catch (UserNotFoundException $e) {
      echo ERR_USER_DOES_NOT_EXIST;
    }
  }
  // Add location
  if ($user['lat']) {
    $location = new UserLocation();
    $location->userId = $usr->id;

    if ($user['is_business']) {
      $location->isBusiness = true;
      $location->type = 'p'; // Business location is always primary
      $location->business = new BusinessLocation();
      $location->business->categoryId = 0; // TODO: Implement business categories
    } else {
      $location->isBusiness = false;
      if ($user['expire'] == 0) {
        $location->type = 'p';
      } else {
        $location->type = 't';
        //Set expiration date
        $location->expire = date(DEFAULT_DATE_FORMAT, $user['expire']);
      }
    }

    // Set location coordinates
    $location->lat = $user['lat'];
    $location->lng = $user['lng'];

    // Set location name, country, address
    $location->name = $user['name'];
    $location->countryISO = strtoupper($user['abr']);
    $location->city = $user['city'];
    $location->googleAddress = $user['address'];
    $location->userAddress = $user['address'];

    if ($user['is_default']) {
      $location->isDefault = true;
    }
    Reg::get('usrLocationMgr')->addUserLocation($location);
    $locationsAdded++;
  }
}

echo "\nUsers created: ". $createdUsersCount;
echo "\nLocations added: ". $locationsAdded;