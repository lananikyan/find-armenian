<?php
if(!empty($_GET['sc']) and !empty($_GET['sn'])){
  $usersFilter = new SiteUserFilter();
  $usersFilter->setLoginLike($_GET['sn']);

  $locationFilter = new UserLocationFilter();
  $locationFilter->setDefault();
  $locationFilter->setCountryISO($_GET['sc']);

  $fm = new FilterMerger($locationFilter);
  $fm->mergeLeft($usersFilter);

  $locations = Reg::get('usrLocationMgr')->getLocations($locationFilter, new MysqlPager(7),UserLocationManager::INIT_NONE);

  $users = array();
  foreach ($locations as $location){
    $userLocation = array();
    $userLocation['userId'] = $location->userId;
    $userLocation['googleAddress'] = $location->googleAddress;
    $userLocation['userAddress'] = $location->userAddress;

    $user = Reg::get('userMgr')->getUserById($location->userId, UserManager::INIT_PROPERTIES);
    $userLocation['gravatarCode'] = md5( strtolower( trim( $user->email ) ) );
    $userLocation['login'] = $user->login;
    $userLocation['creationDate'] = $user->creationDate;

    $users[] = $userLocation;
  }

  // Todo:: Get locations
  Reg::get("smarty")->assign("users", $users);

}
else{
  Reg::get('error')->add("Empty name or country");
}
Reg::get("smarty")->assign("countries", Reg::get('usrLocationMgr')->countries());