<?php
if(!empty($_GET['uid'])){
  try{
    $user = Reg::get('userMgr')->getUserById($_GET['uid'], UserManager::INIT_PROPERTIES, 2);
    $locationFilter = new UserLocationFilter();
    $locationFilter->setUserId($user->id);
    //$locationFilter->setDefault();
    $locations = Reg::get('usrLocationMgr')->getLocations($locationFilter);
    foreach($locations as $location){
      if($location->isDefault){
        $defaultLocation = $location;
        break;
      }
    }
    Reg::get("smarty")->assign("user", $user);
    Reg::get("smarty")->assign("location", $defaultLocation);
    Reg::get("smarty")->assign("locations", $locations);
    Reg::get("smarty")->addCss('user.css');
    Reg::get('smarty')->addJs("message.js");
  }
  catch(Exception $e){
    if(isInDevelopmentMode()){
      Reg::get('error')->add($e->getMessage());
    }
    Reg::get('error')->add("Wrong user id");
    redirect(SITE_PATH);
  }
}
else{
  redirect(SITE_PATH);
}