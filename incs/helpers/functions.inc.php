<?php
/**
 * Do post creation actions,
 *
 * @param User $user
 * @param bool $redirectTo
 * @param bool $withOutput
 */
function userCreationActions(User $user, $redirectTo = SITE_PATH, $withOutput = true, $withRedirect = true)
{
	if ($withOutput) {
		Reg::get('info')->add(REGISTER_SUCCESS);
	}

	// Send welcome e-mail
	$content = Reg::get('smarty')->getChunk("mails/welcome.tpl", array('user'=>$user));
	$email = Reg::get('smarty')->getChunk("mails/template.tpl", array('mailTitle'=>"Welcome to FindArmenian", 'mailContent'=>$content));

	if( mail($user->email, "Welcome to FindArmenian", $email, mailHeaders()) ){
		Reg::get('info')->add("Email is sent to: " . $user->email);
	}
	else{
		Reg::get('error')->add("Can't send e-mail to: " . $user->email);
	}

	if (isset($_SESSION['guestLocation'])) {
		$location = unserialize($_SESSION['guestLocation']);
		$location->userId = $user->id;
		$location->isDefault = true;
		Reg::get('usrLocationMgr')->addUserLocation($location);
		unset($_SESSION['guestLocation']);
	}

	doLoginDirect($user);

	if ($withRedirect) {
		redirect($redirectTo);
	}
}

/**
 * Log in user directly by object
 * @param User $user
 * @param array $additionalCredentials
 * @param bool $remember
 */
function doLoginDirect(User $user, $additionalCredentials = array(), $remember = false)
{
	try {
		$usr = Reg::get('userAuth')->doLogin($user->id, $additionalCredentials, $remember);
		Reg::register("usr", $usr, true);
	} catch (UserAuthFailedException $e) {
		Reg::get('error')->add(INC_LOGIN_PASS);
		redirect(SITE_PATH);
	}
}

function is_logined()
{
	if (!isAuthorized()) {
		if (empty($_GET["ajax"])) {
			$_SESSION['redirect_after_login'] = $_SERVER["REQUEST_URI"];
			Reg::get('error')->add(ERR_YOU_HAVE_TO_LOGIN);
		}
		redirect(SITE_PATH);
	}
	return true;
}


function postLoginOperations()
{

	if (isAuthorized()) {
		// Check user's preffered host. If user login from another host, set it as preffered
		if (Reg::get('usr')->props->hostId != Reg::get('host')->id) {
			Reg::get('usr')->props->hostId = Reg::get('host')->id;
		}

		// Check user's preffered language. If user login using another language, set it as preffered
		if (Reg::get('usr')->props->langId != Reg::get('language')->id) {
			Reg::get('usr')->props->langId = Reg::get('language')->id;
		}

		// Update user's last IP
		if (Reg::get('usr')->lastLoginIP != $_SERVER['REMOTE_ADDR']) {
			Reg::get('usr')->lastLoginIP = $_SERVER['REMOTE_ADDR'];
		}

		if (isset($_SESSION['guestLocation'])) {
			$location = unserialize($_SESSION['guestLocation']);
			$location->userId = Reg::get('usr')->id;
			//If user has no default location set this one as default
			$defaultLocation = Reg::get('usrLocationMgr')->getUserDefaultLocation(Reg::get('usr'));
			if (empty($defaultLocation)) {
				$location->isDefault = true;
			}
			Reg::get('usrLocationMgr')->addUserLocation($location);
			unset($_SESSION['guestLocation']);
		}

		Reg::get('userMgr')->updateUser(Reg::get('usr'));
	}

	if (isset($_SESSION['redirect_after_login']) and !empty($_SESSION['redirect_after_login'])) {
		$redirectUrl = $_SESSION['redirect_after_login'];
		unset($_SESSION['redirect_after_login']);
		redirect($redirectUrl);
	} else {
		redirect(SITE_PATH);
	}
}

/**
 * Delete entries from memcache for given classes
 *
 * @param array|string $keys
 * @return integer
 */
function deleteMemcacheKeys($classes, $customGlobalPrefix = null)
{
	if (!ConfigManager::getConfig("Db", "Memcache")->AuxConfig->enabled) {
		return 0;
	}

	if (!is_array($classes)) {
		$classes = array($classes);
	}

	if ($customGlobalPrefix !== null) {
		$globalPrefix = $customGlobalPrefix;
	} else {
		$globalPrefix = ConfigManager::getConfig("Db", "Memcache")->AuxConfig->keyPrefix;
	}

	$memcacheConfig = ConfigManager::getConfig('Db', 'Memcache')->AuxConfig;
	$memcached = new MemcacheWrapper($memcacheConfig->host, $memcacheConfig->port);

	$list = $memcached->getKeysList();

	$count = 0;
	foreach ($list as $key) {
		$array = explode(":", $key);
		if (count($array) == 3) {
			if ($array[0] == $globalPrefix and in_array($array[1], $classes)) {
				if ($memcached->delete($key)) {
					$count++;
				}
			}
		}
	}

	return $count;
}

/**
 * Compose e-mail header
 * @return string
 */
function mailHeaders(){

	$config = ConfigManager::getGlobalConfig();

	$headers = array();
	$headers[] = "MIME-Version: 1.0";
	$headers[] = 'Content-Type: text/html; charset="UTF-8";';
	$headers[] = "From: FindArmenian <" . $config->site->info_mail . ">";
	$headers[] = "Reply-To: " . $config->site->norely_mail;
	$headers[] = "Return-Path: <" . $config->site->info_mail . ">";
	$headers[] = "X-Mailer: PHP/" . phpversion();

	return implode("\r\n", $headers);
}

/**
 * Generate code to start doing forget password
 * @param User $usr
 * @return mixed
 */
function generateForgetPassCode(User $usr){
	$config = new OTCConfig();
	$config->paramsArray = array('t'=>'fp', 'uid' => $usr->id);
	$config->multiUse = true;
	$config->usageLimit = 4;
	return Reg::get('otc')->generate($config);
}

/**
 * Validate given code
 * @param $code
 * @return mixed bool|User
 */
function validateForgotPassCode($code){
	$actUsrId = null;
	if(Reg::get('otc')->validate($code, array('t'=>'fp'))){
		$extractedCode = Reg::get('otc')->getArrayFromCode($code);
		$userId = $extractedCode['uid'];
		try{
			$user = Reg::get('userMgr')->getUserById($userId, UserManager::INIT_NONE);
		}
		catch(UserNotFoundException $e){
			return false;
		}
		return $user;
	}
	return false;
}

/**
 * TODO: Add comment !
 */
function removeForgetPassCode($code){
	return Reg::get('otc')->revokeCode($code);
}

/**
 * @param $newUsersLocations
 * @param $allSexes
 * @return string
 */
function create_notification_email($newUsersLocations, $allSexes=false){
  $emailHTML = Reg::get('smarty')->getChunk("mails/notification.tpl", array('usersLocations'=>$newUsersLocations, 'allSexes'=>$allSexes));
  return $emailHTML;
}