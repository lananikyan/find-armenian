<?php
class RewriteCustomAliasURL extends RewriteAliasURL{
	
	/**
	 * @return string
	 */
	protected function parseCustomAliases($uri){
		$uri = $this->parseCustom($uri);
		$uri = $this->parseCountry($uri);
		return $uri;
	}
	
	private function parseCustom($uri){
		
		self::ensureLastSlash($uri);
		
		// Do something with $uri
		
		return $uri;
	}

	private function parseCountry($uri){
		$explodedUri = explode("/", $uri);
		if(Reg::get('usrLocationMgr')->isCountry($explodedUri[0])){
			$uri = '/home/address:'.$explodedUri[0];
      $country = Reg::get('usrLocationMgr')->getCountryByName($explodedUri[0]);
      $_GET['country'] = $country;
		}
		return $uri;
	}
}
