<?php
class SiteUserFilter extends UsersFilter{


	public function setAlertBoy(){
		$this->joinUsersPropertiesTable();
		$this->qb->andWhere($this->qb->expr()->equal(new Field('email_alert_boy', "users_prop"), '1'));
		return $this;
	}

	public function setAlertGirl(){
		$this->joinUsersPropertiesTable();
		$this->qb->andWhere($this->qb->expr()->equal(new Field('email_alert_girl', "users_prop"), '1'));
		return $this;
	}

	/** Set user sex
	 * @param $sexes
	 * @return $this
	 * @throws InvalidArgumentException
	 */
	public function setSex($sexes){
		if(empty($sexes)){
			throw new InvalidArgumentException("\$sexes have to be non empty string or array");
		}

		if(!is_array($sexes)){
			$sexes = array($sexes);
		}

		$this->joinUsersPropertiesTable();
		if(count($sexes)==1){
			$this->qb->andWhere($this->qb->expr()->equal(new Field("sex", "users_prop"), $sexes[0]));
		}
		else{
			$this->qb->andWhere($this->qb->expr()->in(new Field("sex", "users_prop"), $sexes));
		}
		return $this;
	}

  /**
   * Set email or login equal to given string
   * @param  $emailOrUsername
   * @return $this
   * @throws InvalidArgumentException
   */
  public function setEmailOrUsername($emailOrUsername){
    if(empty($emailOrUsername)){
      throw new InvalidArgumentException("\email or username argument have to be non empty string or array");
    }

    $orX = new Orx();
    $orX->add($this->qb->expr()->equal(new Field('email', $this->primaryTableAlias), $emailOrUsername));
    $orX->add($this->qb->expr()->equal(new Field('login', $this->primaryTableAlias), $emailOrUsername));
    $this->qb->andWhere($orX);

    return $this;
  }
}