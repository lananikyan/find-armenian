<?php
class UserLocationFilter extends MergeableFilter{

	public function __construct(){
		parent::__construct(Tbl::get('TBL_USER_LOCATION', 'UserLocationManager'), "user_location", "user_id");

		$this->qb->select(new Field('*', $this->primaryTableAlias))->from($this->primaryTable, $this->primaryTableAlias);
	}

	/**
	 * @param $userId
	 * @throws InvalidArgumentException
	 */
	public function setUserId($userId){
		if(empty($userId) or !is_numeric($userId)){
			throw new InvalidArgumentException("Invalid user ID specified!");
		}

		$this->qb->andWhere($this->qb->expr()->equal(new Field("user_id", $this->primaryTableAlias), $userId));
	}

	/**
	 * @param $userIds
	 * @return $this
	 * @throws InvalidArgumentException
	 */
	public function setUserIdIn($userIds){
		if(empty($userIds) or !is_array($userIds)){
			throw new InvalidArgumentException("\$userIds have to be non empty array");
		}

		$this->qb->andWhere($this->qb->expr()->in(new Field("user_id", $this->primaryTableAlias), $userIds));
		return $this;
	}

	/**
	 * Set location ID
	 * @param $id
	 * @throws InvalidArgumentException
	 */
	public function setId($id){
		if(empty($id) or !is_numeric($id)){
			throw new InvalidArgumentException("Invalid ID specified!");
		}

		$this->qb->andWhere($this->qb->expr()->equal(new Field("id", $this->primaryTableAlias), $id));
	}

	/**
	 * Set city name
	 * @param $city
	 * @throws InvalidArgumentException
	 */
	public function setCity($city){
		if(empty($city)){
			throw new InvalidArgumentException("Invalid city specified!");
		}
		$this->qb->andWhere($this->qb->expr()->equal(new Field("city", $this->primaryTableAlias), $city));
	}

	/**
	 * Set country ISO
	 * @param $countryISO
	 * @throws InvalidArgumentException
	 */
	public function setCountryISO($countryISO){
		if(empty($countryISO)){
			throw new InvalidArgumentException("Invalid country ISO specified!");
		}
		$this->qb->andWhere($this->qb->expr()->equal(new Field("countryISO", $this->primaryTableAlias), $countryISO));
	}

	/**
	 * Select default addresses only
	 */
	public function setDefault(){
		$this->qb->andWhere($this->qb->expr()->equal(new Field("is_default", $this->primaryTableAlias), '1'));
	}

	/**
	 * Set creation date
	 * @param string $date in DEFAULT_DATE_FORMAT
	 * @return UserLocationFilter
	 */
	public function setDateGreaterEqual($date){
		$this->qb->andWhere($this->qb->expr()->greaterEqual(new Field('date'), $date));
		return $this;
	}

	/**
	 * Set creation time
	 * @param string $time in DEFAULT_TIME_FORMAT
	 * @return UserLocationFilter
	 */
	public function setTimeGreaterEqual($time){
		$this->qb->andWhere($this->qb->expr()->greaterEqual(new Field('time'), $time));
		return $this;
	}

	/**
	 * Set creation date less
	 * @param string $date in DEFAULT_DATE_FORMAT
	 * @return UserLocationFilter
	 */
	public function setDateLess($date){
		$this->qb->andWhere($this->qb->expr()->less(new Field('date'), $date));
		return $this;
	}

	/**
	 * Set creation time less
	 * @param string $time in DEFAULT_TIME_FORMAT
	 * @return UserLocationFilter
	 */
	public function setTimeLess($time){
		$this->qb->andWhere($this->qb->expr()->less(new Field('time'), $time));
		return $this;
	}
}