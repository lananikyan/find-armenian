<?php
class LoaderUserLocation extends Loader{
	
	protected function includes(){
		require_once ('Managers/UserLocationManager.class.php');
		require_once ('Objects/UserLocation.class.php');
		require_once ('Objects/BusinessLocation.class.php');
		require_once ('Filters/UserLocationFilter.class.php');
		require_once ('Exceptions/UserLocationException.class.php');
		require_once ('Exceptions/UserLocationNotExists.class.php');
	}
	
	protected function customInitBeforeObjects(){
		Tbl::registerTableNames('UserLocationManager');
	}
	
	protected function loadUserLocationManager(){
		$this->register(new UserLocationManager());
	}
}
