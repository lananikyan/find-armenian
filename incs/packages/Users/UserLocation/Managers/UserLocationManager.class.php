<?php
class UserLocationManager extends DbAccessor
{

	const TBL_USER_LOCATION = "user_location";
	const TBL_USER_LOCATION_BUSINESS = "user_location_business";
	const TBL_COUNTRY = "country";

	const INIT_NONE = 0;
	// Init flags needs to be powers of 2 (1, 2, 4, 8, 16, 32, ...)
	const INIT_OBJECTS = 1;
	// INIT_ALL Should be next power of 2 minus 1
	const INIT_ALL = 3;

	/**
	 * @param UserLocationFilter $filter
	 * @param MysqlPager $pager
	 * @param int $cacheMinutes
	 * @return array
	 */
	public function getLocations(UserLocationFilter $filter = null, MysqlPager $pager = null, $initObjects = self::INIT_NONE, $cacheMinutes = 0)
	{
		if ($filter == null) {
			$filter = new UserLocationFilter();
		}
		$sqlQuery = $filter->getSQL();
		//echo $sqlQuery;
		if ($pager !== null) {
			$this->query = $pager->executePagedSQL($sqlQuery, $cacheMinutes);
		} else {
			$this->query->exec($sqlQuery, $cacheMinutes);
		}
		$locations = array();
		if ($this->query->countRecords()) {
			while (($row = $this->query->fetchRecord()) != null) {
				array_push($locations, $this->getUserLocationFromData($row, $initObjects, $cacheMinutes));
			}
		}
		return $locations;
	}

	/**
	 * Get one location
	 * @param UserLocationFilter $filter
	 * @param int $cacheMinutes
	 * @return UserLocation
	 * @throws UserLocationException
	 */
	public function getLocation(UserLocationFilter $filter = null, $initObjects = self::INIT_NONE, $cacheMinutes = 0)
	{
		$locations = $this->getLocations($filter, null, $initObjects, $cacheMinutes);
		if (count($locations) > 1) {
			throw new UserLocationException("Location is not unique.");
		} elseif (count($locations) < 1) {
			throw new UserLocationNotExists("There is no such location");
		} else {
			return $locations[0];
		}
	}

	/**
	 * @param int $locationId
	 * @param int $cacheMinutes
	 * @return UserLocation
	 * @throws UserLocationException
	 */
	public function getLocationById($locationId, $initObjects = self::INIT_NONE, $cacheMinutes = 0)
	{
		$filter = new UserLocationFilter();
		$filter->setId($locationId);

		$locations = $this->getLocations($filter, null, $initObjects, $cacheMinutes);
		if (count($locations) == 0) {
			throw new UserLocationNotExists("No location with given Id '$locationId'");
		} elseif (count($locations) > 1) {
			throw new UserLocationException("Given location Id is not unique");
		} else {
			return $locations[0];
		}
	}

	/**
	 * @param UserLocation $userLocation
	 * @return UserLocation
	 */
	public function addUserLocation(UserLocation $userLocation)
	{
		$qb = new QueryBuilder();

		$qb->insert(Tbl::get("TBL_USER_LOCATION"))
			->values(
				array(
					'user_id' => $userLocation->userId,
					'name' => $userLocation->name,
					'type' => $userLocation->type,
					'lat' => $userLocation->lat,
					'lng' => $userLocation->lng,
					'city' => $userLocation->city,
					'countryISO' => $userLocation->countryISO,
					'google_address' => $userLocation->googleAddress,
					'user_address' => $userLocation->userAddress,
					'date' => new Func('NOW'),
					'time' => new Func('NOW'),
					'expire' => $userLocation->expire,
					'is_default' => strval($userLocation->isDefault ? 1 : 0),
					'is_business' => strval($userLocation->isBusiness ? 1 : 0)
				)
			);
		$this->query->exec($qb->getSQL());
		$userLocation->id = $this->query->getLastInsertId();

		// Add business data
		if($userLocation->isBusiness){
			$qb->insert(Tbl::get("TBL_USER_LOCATION_BUSINESS"))
				->values(
					array(
						'location_id' => $userLocation->id,
						'category_id' => $userLocation->business->categoryId,
						'phone' => $userLocation->business->phone,
						'email' => $userLocation->business->email,
						'url' => $userLocation->business->url,
					)
				);
			$this->query->exec($qb->getSQL());
			$userLocation->business->setId($this->query->getLastInsertId());

		}
		return $userLocation;
	}

	/**
	 * Update user location
	 * @param UserLocation $userLocation
	 * @return bool
	 */
	public function updateUserLocation(UserLocation $userLocation)
	{
		$qb = new QueryBuilder();

		$qb->update(Tbl::get('TBL_USER_LOCATION'))
			->set(new Field('name'), $userLocation->name)
			->set(new Field('type'), $userLocation->type)
			->set(new Field('lat'), $userLocation->lat)
			->set(new Field('lng'), $userLocation->lng)
			->set(new Field('city'), $userLocation->city)
			->set(new Field('countryISO'), $userLocation->countryISO)
			->set(new Field('google_address'), $userLocation->googleAddress)
			->set(new Field('user_address'), $userLocation->userAddress)
			->set(new Field('expire'), $userLocation->expire)
			->set(new Field('is_default'), strval($userLocation->isDefault ? 1 : 0))
			->set(new Field('is_business'), strval($userLocation->isBusiness ? 1 : 0))
			->where($qb->expr()->equal(new Field('id'), $userLocation->id));

		return $this->query->exec($qb->getSQL())->affected();
	}

	/**
	 * Delete user location
	 * @param $id
	 * @return Integer
	 */
	public function deleteUserLocation($id)
	{
		$qb = new QueryBuilder();

		$qb->delete(Tbl::get('TBL_USER_LOCATION'))
			->where($qb->expr()->equal(new Field('id'), $id));

		return $this->query->exec($qb->getSQL())->affected();
	}

	/**
	 * Get default location of the user
	 * @param User $user
	 * @return UserLocation
	 */
	public function getUserDefaultLocation(User $user)
	{
		try {
			$filter = new UserLocationFilter();
			$filter->setUserId($user->id);
			$filter->setDefault();
			return Reg::get('usrLocationMgr')->getLocation($filter);
		} catch (UserLocationNotExists $e) {
			return null;
		}
	}

	/**
	 * Get list of countries ordered (DESC) by users in the country
	 * @param $limit optional parameter to set limit
	 * @param int $cacheMinutes
	 * @return array
	 */
	public function getUsersByCountriesList($limit = null, $cacheMinutes = 0)
	{

		$usersByCountries = array();
		$qb = new QueryBuilder();
		$qb->select(
			$qb->expr()->count(new Field('user_id'), 'count'),
			new Field('countryISO', 'location'),
			new Field('name_en', 'country', 'country_name')
		)
			->from(Tbl::get("TBL_USER_LOCATION"), 'location')
			->leftJoin(Tbl::get('TBL_COUNTRY'), 'country', $qb->expr()->equal(new Field('countryISO', 'location'), new Field('iso2', 'country')))
			->where($qb->expr()->equal(new Field("is_default"), '1'))
			->groupBy(new Field('countryISO'))
			->orderBy(new Field("count"), MySqlDatabase::ORDER_DESC)
			->limit($limit);

		$this->query->exec($qb->getSQL(), $cacheMinutes);
		if ($this->query->countRecords()) {
			while (($row = $this->query->fetchRecord()) != null) {
				array_push($usersByCountries, $row);
			}
		}
		return $usersByCountries;
	}

  /**
   * Get last registered users in the country
  */
  public function getUsersInCountry($limit = null, $countryISO = null, $cacheMinutes = 0)
  {
    $usersInCountry = array();
    $qb = new QueryBuilder();
    $qb->select(
      new Field('*', 'location'),
      new Field('login', 'usr')
    )
      ->from(Tbl::get("TBL_USER_LOCATION"), 'location')
      ->leftJoin(Tbl::get('TBL_USERS', 'UserManager'), 'usr', $qb->expr()->equal(new Field('user_id', 'location'), new Field('id', 'usr')))
      ->andWhere($qb->expr()->equal(new Field("is_default"), '1'))
      ->andWhere($qb->expr()->equal(new Field("enabled"), '1'))
      ->orderBy(new Field("date"), MySqlDatabase::ORDER_DESC)
      ->limit($limit);

    if($countryISO !== null){
      $qb->andWhere($qb->expr()->equal(new Field("countryISO"), $countryISO));
    }

    $this->query->exec($qb->getSQL(), $cacheMinutes);
    if ($this->query->countRecords()) {
      while (($row = $this->query->fetchRecord()) != null) {
        array_push($usersInCountry, $row);
      }
    }
    return $usersInCountry;
  }

	/**
	 * Check if provided string is a country name
	 * @param $str
	 * @return bool
	 */
	public function isCountry($str)
	{
		$qb = new QueryBuilder();
		$qb->select($qb->expr()->count(new Field('id'), 'count'))
			->from(Tbl::get('TBL_COUNTRY'))
			->where($qb->expr()->equal(new Field("name_en"), $str));
		$this->query->exec($qb->getSQL(), -1);
		if ($this->query->fetchField('count') > 0) {
			return true;
		}
		return false;
	}

  /**
   * Get country by iso code.
   * @param string $isoCode iso code, three or two digits
   * @param null $cacheMinutes
   * @return array
   * @throws InvalidIntegerArgumentException
   */
  public function getCountryByCode($isoCode, $cacheMinutes = null){
    $qb = new QueryBuilder();
    $qb->select(new Field('*'))
      ->from(Tbl::get('TBL_COUNTRY'));

    if(strlen($isoCode) == 2){
      $qb->andWhere($qb->expr()->equal(new Field('iso2'), strtoupper($isoCode)));
    }
    elseif (strlen($isoCode) == 3){
      $qb->andWhere($qb->expr()->equal(new Field('iso3'), strtoupper($isoCode)));
    }
    else{
      throw new InvalidIntegerArgumentException("isoCode should be two or three chars length");
    }
    $this->query->exec($qb->getSQL(), $cacheMinutes);
    return $this->query->fetchRecord();
  }

  /**
   * get country by name
   * @param string $countryName
   * @param null $cacheMinutes
   * @return array
   */
  public function getCountryByName($countryName, $cacheMinutes = null){
    $qb = new QueryBuilder();
    $qb->select(new Field('*'))
      ->from(Tbl::get('TBL_COUNTRY'))
      ->andWhere($qb->expr()->equal(new Field('name_en'), $countryName));

    $this->query->exec($qb->getSQL(), $cacheMinutes);
    return $this->query->fetchRecord();
  }

  /**
   * @param null $cacheMinutes
   * @return array
   */
  public function countries($cacheMinutes = null){
    $qb = new QueryBuilder();
    $qb->select(new Field('*'))
      ->from(Tbl::get('TBL_COUNTRY'));
    $this->query->exec($qb->getSQL(), $cacheMinutes);
    return $this->query->fetchRecords();
  }

	protected function getBusinessLocationObject($locationId, $cacheMinutes){
		if(empty($locationId) or !is_numeric($locationId)){
			throw new InvalidArgumentException("\$locationId have to be non zero integer");
		}

		$qb = new QueryBuilder();
		$qb->select(new Field('*'))
			->from(Tbl::get('TBL_USER_LOCATION_BUSINESS'))
			->where($qb->expr()->equal(new Field('location_id'), $locationId));
		$this->query->exec($qb->getSQL(), $cacheMinutes);

		$businessLocation = new BusinessLocation();

		if($this->query->countRecords()){
			$data = $this->query->fetchRecord();
			$businessLocation->phone = $data['phone'];
			$businessLocation->email = $data['email'];
			$businessLocation->url = $data['url'];
			$businessLocation->categoryId = $data['category_id'];
		}
		return $businessLocation;
	}

  /**
   * @param $data
   * @param int $initObjects
   * @param $cacheMinutes
   * @return UserLocation
   */
  protected function getUserLocationFromData($data, $initObjects = self::INIT_NONE, $cacheMinutes)
	{
		$userLocation = new UserLocation();
		$userLocation->id = $data['id'];
		$userLocation->userId = $data['user_id'];
		$userLocation->name = $data['name'];
		$userLocation->type = $data['type'];
		$userLocation->lat = $data['lat'];
		$userLocation->lng = $data['lng'];
		$userLocation->city = $data['city'];
		$userLocation->countryISO = $data['countryISO'];
		$userLocation->googleAddress = $data['google_address'];
		$userLocation->userAddress = $data['user_address'];
		$userLocation->dateAdded = $data['date'];
		$userLocation->timeAdded = $data['time'];
		$userLocation->expire = $data['expire'];
		$userLocation->isDefault = $data['is_default'] == "1" ? true : false;
		$userLocation->isBusiness = $data['is_business'] == "1" ? true : false;

		if($userLocation->isBusiness and $initObjects == self::INIT_OBJECTS){
			$userLocation->business = $this->getBusinessLocationObject($userLocation->id, $cacheMinutes);
		}

		return $userLocation;
	}
}