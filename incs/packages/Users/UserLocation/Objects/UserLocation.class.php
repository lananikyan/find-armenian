<?php
class UserLocation {
	public $id;
    public $userId;
    public $name;
    public $type;
    public $lat;
    public $lng;
    public $city;
    public $countryISO;
    public $googleAddress;
    public $userAddress;
    public $dateAdded;
    public $timeAdded;
    public $expire; //Expiration date for temporary location
    public $isDefault;
    public $isBusiness; //Bool
	public $business = null; // BusinessLocation object
}