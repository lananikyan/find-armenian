<div class="find-user">
	<div><a href="javascript:;" onclick="$('#add-location-info').toggle();">How to add new location?</a></div>
	<div id="add-location-info" style="margin-top: 5px; display: none;">
		After you register and login in the website, go to the address you want to add.<br>
		You can do this using the search bar on the top, or just scroll on the map.<br>
		When you find the address click on it. Red mark point should appear on the map. Click on it. <br>
		Complete the form and you are done! <br>
	</div>
</div>