<div class="find-user">
	<form action="{'user/find'|glink}" method="get">
		<input name="sn" class="field size-150" type="text"> <span class="b i">Name</span><br>
		<select name="sc" class="sfield size-200">
			<option value="">- select country -</option>
			{foreach $countries as $country}
				<option value="{$country.iso2}">{$country.name_en}</option>
			{/foreach}
		</select>
		<br>
		<button class="primary-btn upper" type="submit"><b class="i-find">Find User</b></button>
	</form>
</div>