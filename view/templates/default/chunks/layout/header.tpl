<a href="/" class="logo" title="Home - Go to Armenians map">&nbsp;</a>
<h1 class="logo-moto">Find Armenian!</h1>
<div class="share">
	<div class='shareaholic-canvas' data-app='share_buttons' data-app-id='4737587'></div>
</div>
<div class="lang-bar">
	<a href="/home?lang=am" class="flag-icon flag-am" title="Հայերեն">&nbsp;</a>
	<a href="/home?lang=en" class="flag-icon flag-en" title="English">&nbsp;</a>
</div>

{chunk file="google_analytics.tpl"}

{literal}
<div id="fb-root"></div>
<script>(function(d, s, id) {
		var js, fjs = d.getElementsByTagName(s)[0];
		if (d.getElementById(id)) return;
		js = d.createElement(s); js.id = id;
		js.src = "//connect.facebook.net/en_US/all.js#xfbml=1&appId=152986842358";
		fjs.parentNode.insertBefore(js, fjs);
	}(document, 'script', 'facebook-jssdk'));</script>

	<script type="text/javascript">
		//<![CDATA[
		(function() {
			var shr = document.createElement('script');
			shr.setAttribute('data-cfasync', 'false');
			shr.src = '//dsms0mj1bbhn4.cloudfront.net/assets/pub/shareaholic.js';
			shr.type = 'text/javascript'; shr.async = 'true';
			shr.onload = shr.onreadystatechange = function() {
				var rs = this.readyState;
				if (rs && rs != 'complete' && rs != 'loaded') return;
				var apikey = 'b531885e25e4e5c80ed8377af5f4fb8c';
				try { Shareaholic.init(apikey); } catch (e) {}
			};
			var s = document.getElementsByTagName('script')[0];
			s.parentNode.insertBefore(shr, s);
		})();
		//]]>
	</script>

{/literal}

