<form style="display:inline;" onsubmit="return false;" method="get" action="/">
	<span class="nav-padding">
		<input type="text" title="location" class="field size-350" value="" id="location-input" name="location">
		<button class="primary-btn upper" type="submit" id="search-location"><b class="i-find">Search</b></button>
	</span>
</form>
{if $isHomepage}
	<span class="nav-separator top"> </span>
	<button class="secondary-btn upper" id="show-world"><b class="i-world">Show World</b></button>
{/if}

<span class="auth-tools fright">
	<span class="nav-separator top">&nbsp;</span>
    {if isAuthorized()}
        <a href="{'profile'|glink}">Profile</a>&nbsp;<a href="{'auth/action:logout'|glink}">Log out</a>
    {else}
        <a href="{'auth'|glink}">Login</a>&nbsp;<a href="{'auth/registration'|glink}">Registration</a>
    {/if}
</span>