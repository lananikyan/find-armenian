Hi, {$recipient->login}. <br>
{$sender->login} find you on FindArmenian.com site and sent you a message:
<br>
<p style="font-style: italic">
	{$message}
</p>
Do not reply to this e-mail.
To contact with {$sender->login} use this address: {$sender->email}