<ul>
{foreach $usersLocations as $newUserLocation}
	<li>
		{$newUserLocation.user->login} at <a href="http://{$host->host}{'home/address:'|cat:$newUserLocation.location->googleAddress|glink}" target="_blank">{$newUserLocation.location->name}</a>
	</li>
{/foreach}
</ul>
Do not reply to this e-mail.
To contact with new user(s) <a href="http://{$host->host}" target="_blank">go to the site</a>.