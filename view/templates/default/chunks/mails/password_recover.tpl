Dear {$user->login},<br>
This email has been sent to you because you forgot your password.<br>
Please <a href="{$reset_form_url}" target="_blank">click here to set a new password</a>. <br>
If the link above does not work, please copy the link below and paste it into your Internet
browser:<br><br>

{$reset_form_url}

<br><br>
If you did not request a new password, please ignore this email. If you have any further
questions, please contact us.