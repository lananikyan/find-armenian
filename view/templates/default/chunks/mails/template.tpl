<table style="background-color: rgb(237, 241, 245);" data-width="600" dir="ltr" data-mobile="true" border="0" cellpadding="0" cellspacing="0" width="100%">
	<tbody><tr>
		<td width="100%" bgcolor="#eeeeee" style="padding-top:20px;padding-right:0;padding-bottom:80px;padding-left:0;margin:0">
			<table width="600" cellspacing="0" cellpadding="15" border="0" align="center" style="border-top-left-radius:10px;border-top-right-radius:10px">
				<tbody>
				<tr>
					<td width="100%" style="background-color:#6297B2;border-top-left-radius:10px;border-top-right-radius:10px; padding-right:15px; padding-left:15px; padding-top: 8px; padding-bottom: 8px;">
                        <span style="display:block;font-family:Arial,Helvetica,sans-serif;font-size:12px;color:#ffffff;">
                            <webversion>Web Version</webversion>&nbsp;&nbsp;|&nbsp;Update preferences&nbsp;&nbsp;|&nbsp; <unsubscribe>Unsubscribe</unsubscribe>
                        </span>
					</td>
				</tr>
				</tbody>
			</table>
			<table width="600" cellspacing="0" cellpadding="15" border="0" align="center">
				<tbody>
				<tr>
					<td width="100%" style="background-color:#6FACCB; padding: 20px;">
						<span style="display:block;font-family:Arial,Helvetica,sans-serif;font-size:26px;font-weight:bold;color:#ffffff; text-align: center;">FindArmenian.com</span>
					</td>
				</tr>
				</tbody>
			</table>
			<table width="600" cellspacing="0" cellpadding="15" border="0" align="center" style="border-left: 1px solid #CCCCCC;border-right: 1px solid #CCCCCC; border-bottom-left-radius:10px;border-bottom-right-radius:10px">
				<tbody>
				<tr>
					<td bgcolor="#ffffff" align="left" style="padding-top:30px;margin:0">
						<span style="color:#206889;font-weight:normal;font-size:18px;font-family:Arial,Helvetica,sans-serif">{$mailTitle}</span>
					</td>
				</tr>
				<tr>
					<td bgcolor="#ffffff" align="left">
                            <span style="color:#262626;font-weight:normal;font-size:15px;font-family:Arial,Helvetica,sans-serif;line-height:22px">{$mailContent}</span>
					</td>
				</tr>

				<tr>
					<td bgcolor="#ffffff" align="center">
						<table cellspacing="0" cellpadding="0" border="0" align="center" style="width:100%">
							<tbody><tr>
								<td height="1" align="center" style="background:#efefef;padding:0;margin:0"></td>
							</tr>
							</tbody></table>
					</td>
				</tr>
				<tr>
					<td bgcolor="#ffffff" align="left" style="padding-top:0;padding-right:15px;padding-left:15px;">
                                    <span style="color:#555555;font-weight:normal;font-size:15px;font-family:Arial,Helvetica,sans-serif;line-height:22px">
                                        Best,
                                        <br>
                                        FindArmenian Team
                                    </span>
					</td>
				</tr>
				<tr>
					<td bgcolor="#6FACCB" align="left" style="padding-top:0;padding-right:15px;padding-left:15px;border-bottom-right-radius:10px;border-bottom-left-radius:10px">
						<table>
							<tbody>
							<tr>
								<td>
									<span style="color:#FFFFFF;font-weight:normal;font-size:12px;font-family:Arial,Helvetica,sans-serif;line-height:22px">You're receiving this because you submitted to get email from findarmenian.com</span>
                                    <span style="display:block;font-family:Arial,Helvetica,sans-serif;font-size:13px;color:#ffffff;">
                                        Edit your subscription&nbsp;&nbsp;|&nbsp; <unsubscribe>Unsubscribe</unsubscribe>
                                    </span>
								</td>
								<td>
                                    <span style="color:#FFFFFF;font-weight:normal;font-size:14px;font-family:Arial,Helvetica,sans-serif;line-height:22px">
                                    contact@findarmenian.com
                                        <br>
                                        +37491257227
                                    </span>
								</td>
							</tr>
							</tbody>
						</table>
					</td>
				</tr>
				</tbody></table>
		</td>
	</tr>
	</tbody>
</table>
