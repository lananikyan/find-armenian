Hi, {$user->login}. <br>
We are happy to welcom you at FindArmenian portal.
Here is a short list of the features:
<ul>
	<li>Search nearby Armenians</li>
	<li>Add your home or work location</li>
	<li>Add your temporary address during trip or holidays</li>
	<li>Add your business address</li>
	<li>Search nearby Armenian restaurants, shops, communities etc</li>
</ul>