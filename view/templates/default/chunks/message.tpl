<div id="message-dialog-form">
	<form>
		<fieldset>
			<p id="message-header" class="user-fullname"><img class="hide loading" src="{'arrows_load.gif'|img}" alt="Loading" title="Loading">Send message to <span id="toUserName"></span></p>
			<textarea name="message" id="message" placeholder="Type your message here" class="ui-widget-content ui-corner-all" style="width:300px;height:130px;"></textarea>
			<input type="hidden" name="toUserId" id="messageToUserId" value="">
		</fieldset>
	</form>
</div>