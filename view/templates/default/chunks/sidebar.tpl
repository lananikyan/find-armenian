<div class="sidebar-box">
	<div class="sidebar-box-head">{$registeredUsers} Armenians</div>
	<div class="sidebar-box-content">
		{foreach $topCountries as $country}
			<span class="country-flag" style="background-image:url({'flags/'|cat:$country.countryISO|lower|cat:'.gif'|img})">{$country.count}</span>
		{/foreach}
		<a href="{'locations/'|glink}" style="margin-bottom:-7px; display:inline-block; width:20px; height:20px; padding:0px; background:url({'arrow.png'|img}) no-repeat;"></a>
		<span class="sidebar-sep"> </span>
		{if $isHomepage}
			<b class="legend-icon permanent mid">&nbsp;</b> Permanent address<br>
			<b class="legend-icon temporary mid">&nbsp;</b> Temporary address<br>
			<b class="legend-icon i-business mid">&nbsp;</b> Business<br>
			{if isAuthorized()}
				<b class="legend-icon i-myloc mid">&nbsp;</b> My location<br>
			{/if}
		{else}
			<img class="center" src="{'200x200.png'|img}">
		{/if}
	</div>
</div>

{if $isHomepage}
	{chunk file='find_user.tpl'}
{/if}
{chunk file='social.tpl'}
{chunk file='add_location_help.tpl'}
