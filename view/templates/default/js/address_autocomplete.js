var autocomplete;

$('document').ready(function(){

  var input = (document.getElementById('location-input'));
  autocomplete = new google.maps.places.Autocomplete(input);

  $("#search-location").click(function(){
    if($("#location-input").val().length >0 ){
      window.location.href = "/" + "home/address:" + $("#location-input").val();
    }
  });

});