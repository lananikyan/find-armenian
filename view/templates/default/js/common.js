function jNavigateCommonHandler(container, data){
    jsCssLoader(data);
    errorInfoHandler(data);
    if(data['redirect'] != null){
        container.navigate('redirect', data['redirect']);
        return { doNotChangeHash : true };
    }

    if(data['parts'] != null){
        if(data['parts']['main'] != null){
            return data['parts']['main'];
        }
    }
}

function jsCssLoader(data){
    if(data['scripts'] != null){
        jQuery.each(data['scripts'], function(index, item) {
            $.ajax({
                url: item,
                dataType: "script",
                cache: true
            });
        });
    }
    if(data['css'] != null){
        jQuery.each(data['css'], function(index, item) {
            if($("link[href='"+item+"']", "head").length == 0){
                $("<link/>", {
                    rel: "stylesheet",
                    type: "text/css",
                    href: item
                })
                    .appendTo("head");
            }
        });
    }
}
function ajaxErrorHandler(data){
    if(data != null && data['redirect'] != null){
        window.location = data['redirect'];
        throw new Error('Stop');
    }
}

if(typeof String.prototype.trim !== 'function') {
  String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, '');
  }
}
