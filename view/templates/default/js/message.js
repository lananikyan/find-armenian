$('document').ready(function () {

  var message = $("#message");

  $("#message-dialog-form").dialog({
    autoOpen: false,
    height: 300,
    width: 350,
    modal: true,
    buttons: [
      {
        text: "Send",
        id: "send-message-btn",
        click: function () {
          var dlg = $(this);
          $.ajax({
            type: 'POST',
            dataType: "json",
            url: js_site_path + "message/action:sendMessage/ajax:1",
            data: { message: message.val(), messageToUserId: $("#messageToUserId").val() },
            success: function (data) {
              if (data.status == "ok") {
                dlg.dialog("close");
              }
              errorInfoHandler(data);
            },
            beforeSend: function () {
              $("#send-message-btn, #cancel-message-btn").attr('disabled', true);
              $("#message-dialog-form img.loading").show();
            },
            complete: function () {
              $("#send-message-btn, #cancel-message-btn").attr('disabled', false);
              $("#message-dialog-form img.loading").hide();
            }
          });
        }
      },
      {
        text: "Cancel",
        id: "cancel-message-btn",
        click: function () {
          $(this).dialog("close");
        }
      }
    ],
    close: function () {
      message.val("");
    }
  });

});

function sendPM(userName, userId) {
  $("#toUserName").text(userName);
  $("#messageToUserId").val(userId);
  $("#message-dialog-form").dialog("open");
}
