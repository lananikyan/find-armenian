<div class="basic-box" style="width: 100%">
	<div class="basic-box-head">
		<span class="basic-box-title">Reset password</span>
	</div>
	<div class="basic-box-content">
		<form action="{'auth/action:reset_password'|glink}" method="post" id="register_form">
			<input type="hidden" name="c_i" value="{$smarty.get.r_id}"/>
			<input type="hidden" name="key" value="{$formKey->getKey()}">

			<table cellspacing="0" class="form-tbl infinite">
				<tbody>
				<tr>
					<th class="top"><label>{'NEW_PASS'|C}</label></th>
					<td>
						<input type="password" class="field2 size-200" value="" name="password" id="reg-password">

					</td>
				</tr>
				<tr>
					<th class="nowrap top"><label>{'RETYPE_NEW_PASS'|C}</label></th>
					<td>
						<input type="password" class="field2 size-200" value="" name="password2" id="reg-rpassword">

					</td>
				</tr>
				<tr>
					<td>&nbsp;</td>
					<td>
						<p align="center">
						<input type="submit" value="{'SAVE'|C}" class="ui-button primary-btn upper size-200" /><br><br>
						<input type="button" value="{'CANCEL'|C}" class="ui-button c" onclick="document.location='{''|glink}'" />
						</p>
					</td>
				</tr>

				</tbody>
			</table>

			<div class="clear"></div>
		</form>
	</div>
</div>