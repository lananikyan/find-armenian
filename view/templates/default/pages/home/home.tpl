<script>
	var address = "{$smarty.get.address}";
</script>
<div id="map">
	<h1>20 Armenians joined recently{if $smarty.get.country} in {$smarty.get.country.name_en}{/if}</h1>
	<table>
		<tr>
			<th>User</th>
			<th>Location Name</th>
			<th>Address</th>
		</tr>
		{foreach $locations as $location}
			<tr>
				<td><a href="{'user/uid:'|cat:$location.user_id|glink}">{$location.login}</a></td>
				<td>{$location.name}</td>
				<td>{$location.user_address}</td>
			</tr>
		{/foreach}
	</table>
</div>
{chunk file='message.tpl'}