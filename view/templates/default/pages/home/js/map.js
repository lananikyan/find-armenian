var images_path = js_site_path+'view/templates/default/img/';
var pinHome = new google.maps.MarkerImage(
  images_path+'pins/pin1.png',
  new google.maps.Size(28, 40),
  new google.maps.Point(0, 0),
  new google.maps.Point(14, 40)
);
var pinTemp = new google.maps.MarkerImage(
  images_path+'pins/pin2.png',
  new google.maps.Size(28, 40),
  new google.maps.Point(0, 0),
  new google.maps.Point(14, 40)
);

var pinBusiness = new google.maps.MarkerImage(
  images_path+'/pins/pin4.png',
  new google.maps.Size(28, 40),
  new google.maps.Point(0, 0),
  new google.maps.Point(14, 40)
);

var pinGuest = new google.maps.MarkerImage(
  images_path+'pins/pin3.png',
  new google.maps.Size(28, 40),
  new google.maps.Point(0, 0),
  new google.maps.Point(14, 40)
);

var pinMy = new google.maps.MarkerImage(
  images_path+'pins/pin5.png',
  new google.maps.Size(28, 40),
  new google.maps.Point(0, 0),
  new google.maps.Point(14, 40)
);

var shape = {
  coord: [18, 0, 20, 1, 22, 2, 23, 3, 24, 4, 25, 5, 25, 6, 26, 7, 27, 8, 27, 9, 27, 10, 27, 11, 27, 12, 27, 13, 27, 14, 27, 15, 27, 16, 27, 17, 27, 18, 27, 19, 26, 20, 26, 21, 25, 22, 25, 23, 24, 24, 23, 25, 23, 26, 22, 27, 22, 28, 21, 29, 20, 30, 20, 31, 19, 32, 18, 33, 18, 34, 17, 35, 17, 36, 16, 37, 15, 38, 15, 39, 12, 39, 12, 38, 11, 37, 11, 36, 10, 35, 9, 34, 9, 33, 8, 32, 7, 31, 7, 30, 6, 29, 5, 28, 5, 27, 4, 26, 4, 25, 3, 24, 2, 23, 2, 22, 1, 21, 1, 20, 0, 19, 0, 18, 0, 17, 0, 16, 0, 15, 0, 14, 0, 13, 0, 12, 0, 11, 0, 10, 0, 9, 0, 8, 1, 7, 2, 6, 2, 5, 3, 4, 4, 3, 5, 2, 7, 1, 8, 0, 18, 0],
  type: 'poly'
};

var markers = [];

$('document').ready(function () {

  var mapOptions = {
    center: new google.maps.LatLng(50, -10),
    zoom: 3,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  var map = new google.maps.Map(document.getElementById("map"), mapOptions);
  var geocoder = new google.maps.Geocoder();
  var infowindow = new google.maps.InfoWindow();
  var myLocation = new google.maps.Marker({title: "Click to add location"});

  if(address.length > 0){
    fitMapToAddress(address);
  }
  else {
    geoip2.city(
      function (location) {
        var address = location.country.names.en;
        if (!$.isEmptyObject(location.city.names)) {
          address += ", " + location.city.names.en;
        }
        fitMapToAddress(address);
      },
      function (error) {
        console.log(error)
      },
      { timeout: 3000 }
    );
  }

  $.each(pins, function (index, p) {
    addPin(p, map);
  });

  var clustererOptions = {gridSize: 80, maxZoom: 14, ignoreHidden: true, styles: [
    {height: 40, width: 40, url: images_path+"cluster1.png", textColor: "white"},
    {height: 50, width: 50, url: images_path+"cluster10.png", textColor: "white"},
    {height: 60, width: 60, url: images_path+"cluster100.png", textColor: "white"}
  ]};
  var clusterer = new MarkerClusterer(map, markers, clustererOptions);

  autocomplete.bindTo('bounds', map);

  google.maps.event.addListener(autocomplete, 'place_changed', function () {

    var place = autocomplete.getPlace();
    if (!place.geometry) {
      // Inform the user that the place was not found and return.
      return;
    }

    if (place.geometry.viewport) {
      map.fitBounds(place.geometry.viewport);
    } else {
      map.setCenter(place.geometry.location);
      map.setZoom(17);  // Why 17? Because it looks good.
    }
  });

  google.maps.event.addListener(map, "click", function (event) {
    myLocation.setMap(null);
    myLocation.setPosition(event.latLng);
    myLocation.setMap(map);
    infowindow.setContent("Loading...");

    geocoder.geocode({'latLng': event.latLng}, function(results, status) {
      var address = "N/A";
      if (status == google.maps.GeocoderStatus.OK) {
        var itemLocality = '';
        var itemCountry = '';
        $.each(results, function (i, result) {
          if (result.types[0] == 'locality') {
            itemLocality = result.address_components[0].long_name;
          }
          if (result.types[0] == 'country') {
            itemCountry = result.address_components[0].short_name;
          }
        });
        address = results[0].formatted_address;

        $.post(
          js_site_path+"locations/addLocationForm/ajax:1",
          { city: itemLocality, countryAlpha2: itemCountry, address: address, latlng: event.latLng.toUrlValue(7) },
          function (data) {
            if (data.status == "ok") {
              infowindow.setContent(data.parts.main);
            }
            errorInfoHandler(data);
          },
          "json"
        );
      }
    });

  });

  google.maps.event.addListener(myLocation, "click", function () {
    infowindow.open(map, myLocation);
  });

  var formOptions = {
    success: function (response, status, xhr, $form) {
      if (response.status == "ok") {
        myLocation.setMap(null);
        addPin(response.parts.location, map, clusterer);
      }
      ajaxErrorHandler(response);
      errorInfoHandler(response);
    },
    beforeSubmit: function (arr, form, options) {
      form.validate({
        errorPlacement: function (error, element) { },
        rules: {
          name: { required: true, rangelength: [4, 64] },
          address: { required: true, minlength: 6 },
          expirationDate: { required: true }
        }
      });
      return form.valid();
    },
    beforeSend: function(){
      $("#newaddress .primary-btn").attr('disabled', true);
      $("#newaddress img.loading").show();

    },
    complete: function(){
      $("#newaddress .primary-btn").attr('disabled', false);
      $("#newaddress img.loading").hide();
    }
  };

  google.maps.event.addListener(infowindow, 'domready', function () {
    $("#newaddress").ajaxForm(formOptions);

    $('input[name="type"]').click(function () {
      $(".businessField, #expiration").hide();
      if ($(this).val() == 't') {
        $("#expiration").show();
      }
      else if ($(this).val() == 'b') {
        $(".businessField").show();
      }
    });
  });

  $("#search-location").click(function () {
    return false;
  });

  function fitMapToAddress(address){
    geocoder.geocode(
      { 'address': address },
      function (results, status) {
        if (status == google.maps.GeocoderStatus.OK) {
          place = results[0];
          if (place.geometry.viewport) {
            map.fitBounds(place.geometry.viewport);
          } else {
            map.setCenter(place.geometry.location);
            map.setZoom(17);
          }
        }
      }
    );
  }
  $("#show-world").click(function(){
    map.setCenter(new google.maps.LatLng(50, -10));
    map.setZoom(2);
  });
});

var infobox = new InfoBox({
  content: "Loading...",
  maxWidth: 150,
  pixelOffset: new google.maps.Size(-80, -80),
  zIndex: null,
  boxClass: "user-popup",
  infoBoxClearance: new google.maps.Size(10, 10)
});

function addPin(location, map, clusterer) {

  var newLatlng = new google.maps.LatLng(location.lat, location.lng);
  var icon = pinGuest;

  if (location.my)  icon = pinMy;
  else if (location.isBusiness)  icon = pinBusiness;
  else if (location.type == 'p')  icon = pinHome;
  else if (location.type == 't')  icon = pinTemp;

  var newMarker = new google.maps.Marker({
    position: newLatlng,
    icon: icon,
    shape: shape,
    //map: map,
    title: "Click to see more"
  });

  google.maps.event.addListener(newMarker, 'click', function () {
    $.post(
      js_site_path+"locations/locationInfo/ajax:1",
      { locationId: location.id},
      function (data) {
        if (data.status == "ok") {
          infobox.setContent(data.parts.main);
          infobox.open(map, newMarker);
        }
        errorInfoHandler(data);
      },
      "json"
    );
  });
  markers.push(newMarker);

  if(typeof clusterer !== "undefined"){
    clusterer.addMarker(newMarker);
  }
}
