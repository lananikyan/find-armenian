<div class="basic-box">
	<div class="basic-box-head">
		<span class="basic-box-title">About us</span>
		<a class="fright back-to-map b" href="/home/">Back to map</a>
	</div>
	<div class="basic-box-content">

		<p>&nbsp;&nbsp;Findarmenian.com aims to unite Armenians all over the world. It’s a great tool for finding an Armenian fellow regardless of where you are. It does not matter whether you are on a holiday or a business trip; you'll find an Armenian everywhere. With this website Armenians living in Armenia and in the Armenian Diaspora, spread across the world, will become one step closer. It will be possible to share and discuss common problems and issues related to Armenians worldwide.</p>

		<p>&nbsp;&nbsp;Hopefully the site will help people who want to come back or move to Armenia and those Armenian who are abroad. </p>
		<p>&nbsp;&nbsp;The fact that in recent years more and more Armenians from around the world return back home proves that Armenians are really hospitable and open-hearted. So with Findarmenian.com Armenians spread throughout the world can find new friends and acquaintances, making it easier for them to facilitate accommodation in a foreign country.</p>
		<p>&nbsp;&nbsp; On our site you will also be able to communicate and meet with people from your hometown, because people living even in one community are often not aware of each other's existence. The unique features of the site will help you not to feel lonely regardless of where you are.  </p>
		<p>&nbsp;&nbsp;The website notifies you whenever a new person gets registered in your area. However everyone has the option of publishing their personal information to the extent they wish.  All other information is strictly confidential and hidden from other users.
			Please also visit our <a href="https://www.facebook.com/FindArmenian" target="_blank">Facebook page</a>.</p>
		<p style="text-align: right"><i><b>FindArmenian.com</b> was opened on 22 Jan 2013 and is dedicated to Nane Ananikyan</i></p>

	</div>
</div>