<div class="basic-box">
	<div class="basic-box-head">
		<span class="basic-box-title">Contacts</span>
		<a class="fright back-to-map b" href="/home/">Back to map</a>
	</div>
	<div class="basic-box-content">
		<h1>FindArmenian.com</h1>
		<p>
			<b>Address:</b> Yerevan, Armenia<br>
			<b>Phone:</b> 00374 91257227<br>
			<b>Email:</b> <a href="mailto:contact@findarmenian.com">contact@findarmenian.com</a><br>
			<b>Contact Name:</b> Marianna Karapetyan
		</p>
	</div>
</div>