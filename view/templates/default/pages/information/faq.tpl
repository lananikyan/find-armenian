<div class="basic-box">
	<div class="basic-box-head">
		<span class="basic-box-title">Frequently Asked Questions</span>
		<a class="fright back-to-map b" href="/home/">Back to map</a>
	</div>
	<div class="basic-box-content">

		<div class="faq-item">
			<div class="faq-question">How to save my details on the map<b class="faq-state">&nbsp;</b></div>
			<div class="faq-answer">
				In order to save your details, you first need to register a profile, where you can save your details (username, email address, age, etc.)
			</div>
		</div>

		<div class="faq-item">
			<div class="faq-question">How to add my address?<b class="faq-state">&nbsp;</b></div>
			<div class="faq-answer">
				Find the location where you live by typing your address in the search bar and click the Search button. Then click the tiptool next to the red mark point on the map. Complete the form and you are done!
			</div>
		</div>

		<div class="faq-item">
			<div class="faq-question">How to add another address /location to my profile<b class="faq-state">&nbsp;</b></div>
			<div class="faq-answer">
				Find the location where you live by typing your address in the search bar and click the Search button. When you find the address click with the right button on the mouse and follow the menu. On the next page, fill in the date the address will be valid until, if this is a temporary address, and click Add location.<br>To return to the map, click the logo of the site in the top left corner of the page.
			</div>
		</div>

		<div class="faq-item">
			<div class="faq-question">How to make an address default?<b class="faq-state">&nbsp;</b></div>
			<div class="faq-answer">
				When you save an address on your profile, you can click the word “default” next to that address. The map will show your marker at the address.
			</div>
		</div>

		<div class="faq-item">
			<div class="faq-question">Can the site show my real location?<b class="faq-state">&nbsp;</b></div>
			<div class="faq-answer">
				No, the site can only detect and show the location of your IP address, if you choose “track my location”. It cannot show your real address. Only you can give your real address, if you choose to do so.
			</div>
		</div>

		<div class="faq-item">
			<div class="faq-question">How to find other Armenians on the map?<b class="faq-state">&nbsp;</b></div>
			<div class="faq-answer">
				Type a user name in the search box, choose a country from the drop-down menu and click the "search" button.<br>Alternatively, you can browse the map to see where there are other registered users. When you click on the white and green and red marker, you can see the username and the address of the user
			</div>
		</div>

		<div class="faq-item">
			<div class="faq-question">How to zoom in or zoom out?<b class="faq-state">&nbsp;</b></div>
			<div class="faq-answer">
				Use the plus and the minus buttons on the left-hand side of the page to zoom in and zoom out the map. You can also zoom in by a the middle button of the mouse.
			</div>
		</div>

		<div class="faq-item">
			<div class="faq-question">How to see the map of the world?<b class="faq-state">&nbsp;</b></div>
			<div class="faq-answer">
				To zoom out and see the world map, click the button Show the World.
			</div>
		</div>

		<div class="faq-item">
			<div class="faq-question">How can I find my location?<b class="faq-state">&nbsp;</b></div>
			<div class="faq-answer">
				Click the button “Where Am I?” at the top of the map.
			</div>
		</div>

		<div class="faq-item">
			<div class="faq-question">How can I change my personal information or my password?<b class="faq-state">&nbsp;</b></div>
			<div class="faq-answer">
				Log in to your account. Open "Edit Profile" from the drop-down menu below your username and make the necessary changes, then click on the “save changes” button.
			</div>
		</div>

		<div class="faq-item">
			<div class="faq-question">How can I change my password?<b class="faq-state">&nbsp;</b></div>
			<div class="faq-answer">
				Log in to your account. Go to your profile, open "Edit Profile" from the drop-down menu below your username and make the necessary changes, then click on the “save changes” button.
			</div>
		</div>


	</div>
</div>