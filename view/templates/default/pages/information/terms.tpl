<div class="basic-box">
	<div class="basic-box-head">
		<span class="basic-box-title">Terms and conditions for using FindArmenian.com</span>
		<a class="fright back-to-map b" href="/home/">Back to map</a>
	</div>
	<div class="basic-box-content">

		<p>These Terms govern the relationship with the users of the resulting in the provision and use of services.</p>

		<p>"USER" under these conditions is a person who is registered with a unique username and password in website or use a free service open to the public.</p>

		<p>The user agrees to the terms and conditions and rules for use of the site by filling out a registration form, a unique username and password in Findarmenian.com and press "Register".</p>

		<center>RIGHTS AND OBLIGATIONS OF SITE</center>

		<p>Findarmenian has the right (but not the obligation) to install on users' computers cookies (cookies) - small text files that are stored on the website server via the Internet on the user's hard drive and enable recovery of user information it identify.</p>

		<p>Findarmenian services offered "AS IS" and are not responsible for the reliability of their operation, the number and type of included features and access to services. Assumes no obligation to provide assistance and instructions.</p>

		<p>Findarmenian website has the right to change the technology and the design and functionality of the site and the services provided without prior notice.</p>

		<p>Findarmenian have right to limit access to some or all of its users to some of the services or all services.</p>

		<p>Findarmenian have right to close or delete the profile pictures of any user in its sole discretion and without review.</p>

		<p>Findarmenian.com have has the right to display, send, load, execute, exhibits, etc. all promotional materials to our users. The content of all ads, and trademarks, the sole responsibility of the advertisers.</p>

		<p>The fact that Findarmenian is connected to the Internet and should work in a technical environment of the network, determines the inability to ensure that the flow of information to and from Findarmenian have will be monitored and recorded by third parties.</p>

		<p>Findarmenian is not responsible for the content, addresses and pictures published on its pages. Findarmenian not responsible for the actions of their users, even if they have used the services of Findarmenian a lawful or unlawful according to the General Conditions.</p>

		<p>For the provision of their services Findarmenian collect, use and store user names and user-provided email addresses. This information is entered by yourself with the express consent.</p>

		<p>Diagnostic and statistical purposes, to support certain features of the services and needs of advertising, Findarmenian collect information that by itself can not identify a specific individual - date / time of the site visit, IP address, operating system type , resolution, browser type, last visited site etc..</p>

		<p>Information contained in the database Findarmenian, can not be provided to third parties unless this is required by authorities or authorized in accordance with the Bulgarian legislation.</p>

		<p>Links in Findarmenian can refer to other Internet addresses that do not work under these terms and conditions. Findarmenian not responsible for content outside the domain " Findarmenian.com".</p>

		<center>RIGHTS AND OBLIGATIONS OF THE USER</center>

		<p>The user may use the services of Findarmenian in accordance with these Terms and rules.</p>

		<p>The user is fully responsible for maintaining the confidentiality of your username and password and for all activities that occur through use.</p>

		<p>The user is required to provide true and complete data it as personally responsible before the law for all actions carried out by the services Findarmenian.</p>

		<p>The user has access to the information that is entered, except when his photo and profile are disabled non-consecutive terms and conditions or rules.</p>

		<p>User agrees that the staff of Findarmenian depending on their duties, access the following information: email address, IP address, content of messages and more., But no right to disclose / distribute except as permitted in these Terms Terms of cases.</p>

		<p>User is responsible for all attempts to gain unauthorized access to other profiles and directories, software, networking, computers and other devices.</p>


	</div>
</div>