<form id="newaddress" method="POST" action="{'locations/action:addLocation'|glink}">
	<input type="hidden" value="{$smarty.post.latlng}" name="latlng">
	<input type="hidden" value="{$smarty.post.address}" name="googleAddress">
	<input type="hidden" value="{$smarty.post.city}" name="city">
	<input type="hidden" value="{$smarty.post.countryAlpha2}" name="countryAlpha2">
	<table cellspacing="0" class="form-tbl infinite">
		<tbody>
		<tr>
			<th class="top"><label>Location name</label></th>
			<td style="width:390px;">
				<input type="text" class="field2 size-300" value="" name="name">
				<div style="font-size:11px;">Enter name which will be recognisable by you and visible for website users <br> (e.g. Home, Office, Work, Name of your business)</div>
			</td>
		</tr>
		<tr>
			<th class="top"><label>Address</label></th>
			<td>
				<input type="text" class="field2 size-300" value="{$smarty.post.address}" name="address">
			</td>
		</tr>
		<tr>
			<th>Address type</th>
			<td class="mid">
				<label class="pr-10"><input type="radio" name="type" value="p" id="type-p" class="top" checked="checked">Permanent</label>
				<label class="pr-10"><input type="radio" name="type" value="t" id="type-t" class="top">Temporary</label>
				<label class="pr-10"><input type="radio" name="type" value="b" id="type-b" class="top">Business</label>
			</td>
		</tr>
		<tr id="expiration" style="display: none;">
			<th valign="top" class="nowrap"><label>Valid to</label></th>
			<td>
				<input type="text" class="field2 size-100 expirationDate" name="expirationDate" placeholder="MM/DD/YYYY">
				<span style="font-size:11px;">Please write the date when you stop living there</span>
			</td>
		</tr>
		<tr class="businessField" style="display: none;">
			<th valign="top" class="nowrap"><label>Business email</label></th>
			<td>
				<input type="text" class="field2 size-200" name="email">
			</td>
		</tr>
		<tr class="businessField" style="display: none;">
			<th valign="top" class="nowrap"><label>Business phone</label></th>
			<td>
				<input type="text" class="field2 size-200" name="phone">
			</td>
		</tr>
		<tr class="businessField" style="display: none;">
			<th valign="top" class="nowrap"><label>Business URL</label></th>
			<td>
				<input type="text" class="field2 size-200" name="url">
			</td>
		</tr>


		<tr><td colspan="2"><b class="form-tbl-sep">&nbsp;</b></td></tr>
		<tr>
			<th class="mid">&nbsp;<img class="hide loading" src="{'globe_load.gif'|img}"></th>
			<td><button type="submit" class="primary-btn upper size-120">Add location</button></td>
		</tr>
		</tbody></table>
</form>