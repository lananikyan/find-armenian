<span class="user-address">
	{if $location->isBusiness}
	<span class="user-info top"><b class="user-fullname">{$location->name}</b>
		<br>
		{$location->userAddress}
		{if !empty($location->business->url)}
			<br>
			<a href="http://{$location->business->url}" target="_blank">{$location->business->url}</a>
		{/if}
	</span>
	{else}
	<span class="avt-medium top">
		<img src="{$avatar_url}" alt="">
	</span>
		<span class="user-info top"><b class="user-fullname">{$user->login}</b> ({$location->name})
		<br>
		{$location->userAddress}<br>
		{if $location->type == 't'}
			Valid to: {$location->expire|date_format:"%d %B %Y"}
		{/if}
	</span>
	{/if}
</span>
<br>
<span class="user-contacts">
	<span class="inblock pall-10">
		{if $location->isBusiness}
			{if !empty($location->business->phone)}
				Phone: {$location->business->phone}
			{/if}
			{if !empty($location->business->email)}
				<br>
				E-mail: <a href="mailto:{$location->business->email}">{$location->business->email}</a>
			{/if}
		{else}
			<span class="b i">Contacts:</span>
			{if isAuthorized()}
				<a href="javascript:;" onclick="sendPM('{$user->login}', {$user->id});" class="lcontact email">&nbsp;</a>
				{if $user->props->skype != ''}<a href="skype:{$user->props->skype}?call" class="lcontact skype">&nbsp;</a>{/if}
				{if $user->props->facebook != ''}<a href="http://facebook.com/{$user->props->facebook}" target="_blank" class="lcontact facebook">&nbsp;</a>{/if}
				{if $user->props->twitter != ''}<a href="http://twitter.com/{$user->props->twitter}" target="_blank" class="lcontact twitter">&nbsp;</a>{/if}
			{else}
				<a href="{'auth'|glink}">login</a> or <a href="{'auth/registration'|glink}">register</a>
				<br>to see contact information about this user
			{/if}
		{/if}
	</span>
</span>
