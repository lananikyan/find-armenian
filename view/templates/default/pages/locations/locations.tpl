<div class="basic-box">
	<div class="basic-box-head">
		<span class="basic-box-title">Map users by countries</span>
		<a class="fright back-to-map b" href="/home/">Back to map</a>
	</div>
	<div class="basic-box-content">
		<table class="list-tbl" cellspacing="0">
			<tbody>
			{foreach $usersByCountries as $country}
				<tr>
					<td style="width:200px;">
						<span class="country-flag" style="background-image:url({'flags/'|cat:$country.countryISO|lower|cat:'.gif'|img})">&nbsp;</span>
						<a class="country-name" href="{$country.country_name|glink}">{$country.country_name}</a>
					</td>
					<td class="r"><span class="count">{$country.count}</span></td>
				</tr>
			{/foreach}
			</tbody>
		</table>



	</div>
</div>