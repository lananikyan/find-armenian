var langId;
var hostId;
var module;
var page;

$(document).ready(function() {
	$("#textInfo, #status-box").addClass("ui-state-highlight")
	.addClass("ui-corner-all")
	.css("text-align","center");

	$("#add-form").dialog({ autoOpen: false });

	$("#add").button().click(function(){
		$form = $("#add-form");
		var new_module = $("#module", $form).val();
		var new_page = $("#page", $form).val();
		var title = $("#pageTitle", $form).val();
		var keywords = $("#pageKeywords", $form).val();
		var description = $("#pageDescription", $form).val();
		$.post("{'page_info_mgr/action:save/ajax:1'|glink}",
		{
			langId: langId,
			hostId: hostId,
		 	module: new_module,
		 	page: new_page,
		 	infoTitle: title,
		 	infoKeywords: keywords,
		 	infoDescription: description
		},
		function(data) {
      if (data.status == "ok") {
        $("#add-form").dialog('open');
      }
      errorInfoHandler(data);
		});
	});

	$("#add-btn").button().click(function(){
		$("#add-form").dialog('open');
	});

  // Language combo change handler
	$("select#lang-select").change(function(){
    langId = $(this).val();
    $('#page-info-container').html("");
    $("select#page-select").html("").attr('disabled', 'disabled');
    $("select#module-select").html("").attr('disabled', 'disabled');
    if(langId != "all"){
      $(this).add("select#host-select").attr('disabled', 'disabled');
      $.getJSON(
        "{'page_info_mgr/action:get_lang_hosts/ajax:1'|glink}",
        { langId: langId, ajax: 'true' },
        function(response){
          jsonData = response.parts.hosts;
          var options = '<option value="0">Select host</option>';
          options += '<option value="all"> -All hosts- </option>';
          for (var i = 0; i < jsonData.length; i++) {
            options += '<option value="' + jsonData[i].id + '">' + jsonData[i].host + '</option>';
          }
          $("select#host-select").html(options).removeAttr('disabled');
          $("select#lang-select").removeAttr('disabled');
      })
    }
    else{
      $('#page-info-container').html("Loading...");
      $("select#host-select").html("").attr('disabled', 'disabled');
      $.getJSON(
        "{'page_info_mgr/action:get_default/ajax:1'|glink}",
        function(jsonData){
            changeInfoForm(jsonData.parts.pageInfo);
        });
    }
  });

  // Host combo change handler
  $("select#host-select").change(function(){
  	hostId = $(this).val();
  	$('#page-info-container').html("");
  	$("select#page-select").html("").attr('disabled', 'disabled');
  	if(hostId != "all"){
		  $(this).add("select#module-select").attr('disabled', 'disabled');
      $.getJSON(
        "{'page_info_mgr/action:get_modules/ajax:1'|glink}",
        { langId: langId, hostId: hostId },
        function(jsonData){
          var modules = jsonData.parts.modules;
          var options = '<option value="0">Select module</option>';
          options += '<option value="all"> -All modules- </option>';
          for (var i = 0; i < modules.length; i++) {
            options += '<option value="' + modules[i].module + '">' + modules[i].module + '</option>';
          }
          $("select#module-select").html(options).removeAttr('disabled');
          $("select#host-select").removeAttr('disabled');
        }
      );
  	}
  	else{
  		$('#page-info-container').html("Loading...");
  		$("select#module-select").html("").attr('disabled', 'disabled');
  		$.getJSON(
        "{'page_info_mgr/action:get_default/ajax:1'|glink}",
        { langId: langId },
        function(jsonData){
	        changeInfoForm(jsonData.parts.pageInfo);
	      }
      );
  	}
  });

  // Module combo change handler
  $("select#module-select").change(function(){
  	module = $(this).val();
  	$('#page-info-container').html("");
  	if(module != "all"){
      $(this).add("select#page-select").attr('disabled', 'disabled');
      $.getJSON(
        "{'page_info_mgr/action:get_pages/ajax:1'|glink}",
        { langId: langId, hostId: hostId, moduleName: module },
        function(jsonData){
          var pages = jsonData.parts.pages;
          var options = '<option value="0">Select page</option>';
          options += '<option value="all"> -All pages- </option>';
          for (var i = 0; i < pages.length; i++) {
            options += '<option value="' + pages[i].page + '">' + pages[i].page + '</option>';
          }
          $("select#page-select").html(options).removeAttr('disabled');
          $("select#module-select").removeAttr('disabled');
        }
      );
  	}
  	else{
  		$('#page-info-container').html("Loading...");
  		$("select#page-select").html("").attr('disabled', 'disabled');
  		$.getJSON(
        "{'page_info_mgr/action:get_default/ajax:1'|glink}",
        { langId: langId, hostId: hostId },
        function(jsonData){
          changeInfoForm(jsonData.parts.pageInfo);
        }
      );
  	}
  });

  // Page combo change handler
  $("select#page-select").change(function(){
  page = $(this).val();
  $('#page-info-container').html("Loading...");
  if(page != "all"){
  $(this).add("select#page-select").attr('disabled', 'disabled');

    $.getJSON(
      "{'page_info_mgr/action:get_page_info/ajax:1'|glink}",
      { langId: langId, hostId: hostId, moduleName: module, pageName: page },
      function(jsonData){
        changeInfoForm(jsonData.parts.pageInfo);
        $("select#page-select, select#module-select").removeAttr('disabled');
      }
    );
  }
  else{
    $('#page-info-container').html("Loading...");
    $.getJSON(
      "{'page_info_mgr/action:get_default/ajax:1'|glink}",
      { langId: langId, hostId: hostId, moduleName: module },
      function(jsonData){
        changeInfoForm(jsonData.parts.pageInfo);
      }
    );
  }
});

  	$("select#add-lang-select").change(function(){
	langId = $(this).val();

	$(this).add("select#add-host-select").attr('disabled', 'disabled');
    $.getJSON(
    			"{'page_info_mgr/action:get_lang_hosts/ajax:1'|glink}",
    			{ langId: langId, all_hosts: 'true' },
    			function(response){
            var hosts = response.parts.hosts;
      var options = '<option value="0">Select host</option>';
      for (var i = 0; i < hosts.length; i++) {
        options += '<option value="' + hosts[i].id + '">' + hosts[i].host + '</option>';
      }
      $("select#add-host-select").html(options).removeAttr('disabled');
      $("select#add-lang-select").removeAttr('disabled');
    });

  });

  $("select#add-host-select").change(function(){
	hostId = $(this).val();
  });

});

function changeInfoForm(pageInfo){
	var $form = $('#empty-form').clone();

	$("input#save", $form).button().click(function(){
		var title = $("input#pageTitle", $form).val();
		var keywords = $("input#pageKeywords", $form).val();
		var description = $("textarea#pageDescription", $form).val();
		$.post(
      "{'page_info_mgr/action:save/ajax:1'|glink}",
      {
        langId: langId,
        hostId: hostId,
        module: module,
        page: page,
        infoTitle: title,
        infoKeywords: keywords,
        infoDescription: description
      },
      function(data) {
        errorInfoHandler(data);
      });
	});

	if(pageInfo){
		$("#pageTitle", $form).attr("value", pageInfo.title);
		$("#pageKeywords", $form).attr("value", pageInfo.meta_keywords);
		$("#pageDescription", $form).html(pageInfo.meta_description);
	}
	else{
		alert("Not set");
	}
	$('#page-info-container').html($form);
	$($form).show();
}