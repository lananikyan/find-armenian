{literal}
<style type="text/css">
 #page-info-container, #add-value-dialog { width: 530px; margin-top: 10px}
 #page-info-container input, #page-info-container textarea {display:block; margin-bottom:10px; width:100%}
 .add-page-info, .page-info , .page-info fieldset {padding:10px; margin: 10px; }
 .add-page-info input, .add-page-info textarea, .add-page-info select {display:block; margin-bottom:10px; width:100%}
 </style>

{/literal}
<br>
<center>
<div id="textInfo" style="width:600px">
	<h2>Pages title, meta keywords, meta description manger</h2>
	<p>Use filter to view or edit</p>
</div>

<br>

<div id="filter" style="width:800px; text-align: center;">
	<select name="lang" id="lang-select">
	<option value="0">Select language</option>
	<option value="all"> -All languages- </option>
	{foreach from=$allLangs item=language}
		<option value="{$language->id}">{$language->longName}</option>
	{/foreach}
	</select>
	<select name="host" id="host-select" style="width:150px;" disabled="disabled"></select>
	<select name="module" id="module-select" style="width:150px;" disabled="disabled"></select>
	<select name="page" id="page-select" style="width:150px;" disabled="disabled"></select>
</div>


<div id="page-info-container"></div>

<button style="width: 100px" id="add-btn"> Add </button>

<div class="page-info" id="empty-form" style="display:none">
	<fieldset>
		<legend>Page info</legend>
		<label for="pageTitle">Title</label><input type="text" id="pageTitle">
		<label for="pageKeywords">Keywords</label><input type="text" id="pageKeywords">
		<label for="pageDescription">Description</label><textarea id="pageDescription"></textarea>
		<input type="button" id="save" value="Save" style="width:100px">
	</fieldset>
</div>

<div class="add-page-info" id="add-form" style="display:none">
	<fieldset>
		<legend>Add page info</legend>
		<select name="lang" id="add-lang-select">
		<option value="0">Select language</option>
		{foreach from=$allLangs item=language}
			<option value="{$language->id}">{$language->longName}</option>
		{/foreach}
		</select>
		<select name="host" id="add-host-select"  disabled="disabled"></select>
		<label for="pageTitle">module</label><input type="text" id="module">
		<label for="pageTitle">page</label><input type="text" id="page">
		<label for="pageTitle">Title</label><input type="text" id="pageTitle">
		<label for="pageKeywords">Keywords</label><input type="text" id="pageKeywords">
		<label for="pageDescription">Description</label><textarea id="pageDescription"></textarea>
		<input type="button" id="add" value="Add" style="width:100px">
	</fieldset>
</div>
</center>