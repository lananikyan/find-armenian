<div class="address-location" id="location_{$location->id}">
	<b>{$location->name}</b><br>
	<a href="{'home/address:'|cat:$location->googleAddress|glink}">{$location->userAddress}</a>
	{if !$location->isDefault}(<a id="setDefault_{$location->id}" class="setDefault" href="#">make default</a>){else}(default){/if}
	{if $location->type == "t"}
		<br>
		<em>Valid to:</em> {$location->expire|date_format:"%d %B %Y"}
	{/if}
	<a title="Remove address" class="remove-address" onclick="return confirm('Are you sure?')" href="/profile/action:removeAddress/locationId:{$location->id}">&nbsp;</a>
</div>