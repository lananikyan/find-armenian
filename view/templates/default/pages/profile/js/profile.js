$(function() {
    $("#birthdate").datepicker({
        dateFormat: 'mm/dd/yy',
        changeMonth: true,
        changeYear: true,
        yearRange: "-100:-5",
        defaultDate: "-20y"
    });

    $(".basic-box-content").on("click", ".setDefault", function () {
        var linkId = $(this).attr('id');
        $.get("/profile/action:setDefaultLocation/ajax:1/locationId:"+linkId.replace("setDefault_", ""),
            function(data){
                if(data['parts']['update'] != null){
                   var elementId = data['parts']['update']['elementId'];
                    if(data['parts']['update']['replaceWith'] != null){
                        $("#" + elementId).replaceWith(data['parts']['update']['replaceWith']);
                    }
                }
                if(data['status'] == 'ok'){
                    $("#" + linkId).replaceWith("default");
                }
                errorInfoHandler(data);
            }
        );
        return false; // stop the browser following the link
    });

});
