<div class="basic-box">
	<div class="basic-box-head">
		<span class="basic-box-title">Search</span>
		<a class="fright back-to-map b" href="/home/">Back to map</a>
	</div>
	<div class="basic-box-content">

		<div class="search-panel">
			<form method="get" action="{'user/find'|glink}">
				<b>Name</b> <input type="text" value="{$smarty.get.sn}" class="field size-200" name="sn">
				<select name="sc" class="sfield size-200">
					<option value="">- select country -</option>
					{foreach $countries as $country}
						<option value="{$country.iso2}" {if $smarty.get.sc==$country.iso2}selected{/if}>{$country.name_en}</option>
					{/foreach}
				</select>
				<button type="submit" class="primary-btn upper"><b class="i-find">Search</b></button>
			</form>
		</div>

		<table cellspacing="0" class="list-tbl infinite">
			<tbody>
			{foreach $users as $user}
			<tr>
				<td style="width:25px;"><span class="avt-small"><a href="{'user/uid:'|cat:$user.userId|glink}"><img src="http://www.gravatar.com/avatar/{$user.gravatarCode}?d=identicon&s=30" /></a></span></td>
				<td class="mid b"><a href="{'user/uid:'|cat:$user.userId|glink}">{$user.login}</a></td>
				<td class="mid"><b class="i-pin">&nbsp;</b><a href="{'home/address:'|cat:$user.googleAddress|glink}">{$user.userAddress}</a></td>
				<td class="mid r i">{$user.creationDate|date_format}</td>
			</tr>

			{foreachelse}
				<tr>
					<td>
						<div class="msg-warn">
							<b class="icon-warn">&nbsp;</b>
							No users were found!
						</div>
					</td>
				</tr>
			{/foreach}

			</tbody>
		</table>
		<center>
		{if isAuthorized()}
			{draw_pager}
		{else}
			<br>
			<a href="{'auth'|glink}">Login</a> or <a href="{'auth/registration'|glink}">register</a> to see all results.
		{/if}
		</center>
	</div>
</div>
{chunk file='message.tpl'}