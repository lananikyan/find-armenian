<div class="basic-box">
	<div class="basic-box-head">
		<span class="basic-box-title">{$user->login}</span>
		<a class="fright back-to-map b" href="/home/">Back to map</a>
	</div>
	<div class="basic-box-content">
		<div class='user-photo'>
			<img src="http://www.gravatar.com/avatar/{$user->email|trim|strtolower|md5}?d=identicon&s=100" />
		</div>
		<div class='user-data'>
			{if $user->props->name}
				<span class="b i">Name:</span> {$user->props->name}
				<br>
			{/if}
			{if $user->props->birthdate != '0000-00-00'}
				<span class="b i">Age:</span> {$user->props->birthdate|get_age}
				<br>
			{/if}
			<span class="b i">Sex:</span> {if $user->props->sex == 'm'}Male{elseif $user->props->sex == 'f'}Female{/if}
			<br>

			{if isAuthorized()}
				<span class="b i">Location(s):</span>
				<ul>
				{foreach $locations as $l}
					<li>{$l->name}{if $l->isDefault}(default){/if}: <a href="{'home/address:'|cat:$l->googleAddress|glink}">{$l->userAddress}</a>
					{if $l->type == 't'}
						<i>Valid to: {$l->expire|date_format:"%d %B %Y"}</i>
					{/if}
					</li>
				{/foreach}
				</ul>
			{else}
				<span class="b i">Location:</span> ({$location->name})
				<br>
				<a href="{'home/address:'|cat:$location->googleAddress|glink}">{$location->userAddress}</a>
				<br>
				{if $location->type == 't'}
					Valid to: {$location->expire|date_format:"%d %B %Y"}
				{/if}
			{/if}

			<span class="b i">Contacts:</span>
			{if isAuthorized()}
				<a href="javascript:;" onclick="sendPM('{$user->login}', {$user->id});" class="lcontact email">&nbsp;</a>
				{if $user->props->skype != ''}<a href="skype:{$user->props->skype}?call" class="lcontact skype">&nbsp;</a>{/if}
				{if $user->props->facebook != ''}<a href="http://facebook.com/{$user->props->facebook}" target="_blank" class="lcontact facebook">&nbsp;</a>{/if}
				{if $user->props->twitter != ''}<a href="http://twitter.com/{$user->props->twitter}" target="_blank" class="lcontact twitter">&nbsp;</a>{/if}
			{else}
				<a href="{'auth'|glink}">login</a> or <a href="{'auth/registration'|glink}">register</a> to see contact information about this user
			{/if}

		</div>
		<div class='user-other clear'></div>
	</div>
</div>
{chunk file='message.tpl'}